import React, { Component } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { AppRegistry, StyleSheet, Text, Image, View, Alert, TouchableOpacity, Platform, AsyncStorage } from "react-native";
import {
  Scene,
  Router,
  Actions,
  Reducer,
  Dimensions,
  ActionConst,
  Stack,
  Drawer,
  Tabs,
  Modal,
  Lightbox
} from "react-native-router-flux";
import TabIcon from './tabicons';
import ForgetPassword from "./login/forgotpassword";

import OTP from "./login/forgotpassword/otp";
import ResetPassword from "./login/forgotpassword/reset";
import CreateWallet from "./login/forgotpassword/createwallet";
import ResetPasswordConfirm from "./login/forgotpassword/resetconfirm";
import Splash from "./splash/Splash";
import OnBoarding from "./OnBoarding/OnBoarding";

import Home from "./home/driver/home.js";

import Orders from "./home/driver/orders";
import Notifications from "./home/driver/notifications";
import Messages from "./home/driver/messages";

import styles from "./styles.js";
import SideMenu from "./SideMenu.js";
import StyleDict from '../constants/dictStyle'
import icons from '../constants/icons'
const { colorSet } = StyleDict;
//import { Router, Scene } from 'react-native-router-flux';
import { connect, Provider } from "react-redux";
import configureStore from "../store/configureStore";
const store = configureStore();
const RouterWithRedux = connect()(Router);
class App extends Component {
  constructor(props) {
    super(props);
    this.onBackPress = this.onBackPress.bind(this);
  }
   componentDidMount() {
    
  }

  

  componentWillUnmount() {
   
  }
 onBackPress = () => {
    return Actions.pop();
}

  _renderRightButton = () => {
    return (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => this._handleIconTouch()}
      >
        <Icon name="bell" size={26} color={colorSet.white} />
      </TouchableOpacity>
    );
  };

  _handleIconTouch = () => {

  };

  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux backAndroidHandler={this.onBackPress} 
        navBar={()=>null} 
        >
          <Stack
            key="innerroot"
            hideNavBar={true}
           >
            <Drawer
              key="drawer"
              initial
              hideNavBar={true}
              contentComponent={SideMenu}
              drawerImage={icons.hamburger}
              drawerWidth={300}
              drawerOpenRoute='DrawerOpen'
              drawerCloseRoute='DrawerClose'
              drawerToggleRoute='DrawerToggle'
              renderRightButton={() => this._renderRightButton()}
            >
                <Scene
                key="tabs_home"
                >
                <Tabs
                  animationEnabled={false}
                  tabStyle={styles.tab}
                  lazy={true}
                  key="tabs"
                  key="tabBar"
                  swipeEnabled={false}
                  tabBarPosition="bottom"
                  showLabel={false}
                  tabBarStyle={styles.tabBar}
                  activeTintColor={colorSet.blueColor}
                  inactiveTintColor={colorSet.blueColor}
                >
                  <Scene
                    key="home"
                    title="Account"
                    iconName="home"
                    icon={TabIcon}
                    component={Home}
                    
                  />
                  <Scene
                    key="orders"
                    title="Market"
                    iconName="list"
                    icon={TabIcon}
                    component={Orders}
                  />

                  <Scene
                    key="message"
                    component={Messages}
                    title="Exchange"
                    backTitle=""
                    iconName="refresh"
                    icon={TabIcon}
                  />

                  <Scene
                    key="notification"
                    component={Notifications}
                    title="Order"
                    backTitle=""
                    iconName="bell"
                    icon={TabIcon}
                  />
                </Tabs>
                </Scene>
             </Drawer>
           </Stack>

        </RouterWithRedux>
      </Provider>
    );
  }
}
const getDrawerIcon = tintColor => {
  return (
    <Image
      source={require("../res/images/menudrawer.png")}
      style={{ height: 24, width: 24 }}
    />
  );
};

const getDrawertitle = () => {
  return (
    <Image
      source={require("../res/images/NavLogo.png")}
      style={{ height: 24, width: 24 }}
    />
  );
};

export default App;

