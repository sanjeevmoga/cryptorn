import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions,

ImageBackground,
TouchableHighlight,
  AsyncStorage
} from 'react-native';

import {
  Card
} from 'react-native-elements';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";

import { Actions as NavigationActions } from 'react-native-router-flux';

import Paying from './paying'
import Ignored from './ignore'
import CoachList from './feurited/CoachList.js';
//import LoginView from '../login'
//import { authWithPassword, getUserProfile } from '../../helpers/firebase';
//import * as firebase from 'firebase';
//import ActivityIndicator from 'react-native-activity-indicator';
//import { validateEmail } from '../../helpers/validation';

import styles from './styles.js';

class Home extends Component {
  constructor(props) {
    super(props);
        console.disableYellowBox = true;
        this.state={};
        AsyncStorage.getItem("userdata").then((data)=>{

            this.setState(JSON.parse(data));

        });

    // this.state = {
    //   email: "",
    //   password: "",
    //   loaded: false
    // };
  }
  componentDidMount() {

     	// do stuff while splash screen is shown
         // After having done stuff (such as async tasks) hide the splash screen
        // SplashScreen.hide();
     }

  render() {
    return (

 // <ImageBackground source={require('../../res/images/bg.png')} style={styles.container}>



      <View style={styles.container}>

        
    
         <ScrollableTabView
             style={{height:40}}
                   tabBarActiveTextColor="#30385f"
                    tabBarInActiveTextColor="#30385f"
                   tabBarBackgroundColor='transparent'

                   scrollEnabled={ true }
                  renderTabBar={() =>
                 <TabBar underlineColor="#da4383"

                  tabBarStyle={{ alignSelf:'center',width:Dimensions.get('window').width*0.9,backgroundColor: "#fff", borderTopColor: 'grey',borderBottomColor: 'grey', borderLeftColor: 'grey',borderRightColor: 'grey'
                  ,borderBottomWidth: .5,borderTopWidth: .5,borderLeftWidth:.5,borderRightWidth:.5,paddingTop:20}}
                  tabBarTextStyle={{fontSize : 16,fontWeight:'bold',alignSelf:'center',alignItems:'center'}}
                  tabMargin={30}
         /> }>
             <Paying tabLabel={{label: "Paying"}} label="Paying"/>       
           <CoachList tabLabel={{label: "Favourited"}} label="Favourited" /> 
          
           <Ignored tabLabel={{label: "Skipped"}} label="Skipped"/>
           
          
         </ScrollableTabView>

        {/* <TouchableOpacity style={styles.backContainer} onPress={()=>NavigationActions.signup()}>
            <Image source={require('../../res/images/logo.png')} style={styles.backImg}/>
            <Text style={styles.backButton}>Back</Text>
        </TouchableOpacity> */}
      </View>

  //  </ImageBackground>


    );
  }
}

export default Home;
