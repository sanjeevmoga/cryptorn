'use strict'

import {
  StyleSheet,
  Dimensions
} from 'react-native';

var styles = StyleSheet.create({
  container: {
    flex: 1,
      backgroundColor: 'white',

    justifyContent: 'center',
   alignItems: 'center',

  },



  bodyContainer: {

    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 25,
    flexDirection: 'row',
    alignSelf: 'center',


    marginTop:40,
    width:Dimensions.get('window').width*0.8,

      backgroundColor: 'transparent',

    justifyContent: 'center'

  },
  welcome: {


alignSelf:'center',
  height:200,
  width:200
  },


  startText1: {
    fontSize: 20,
    marginTop: 45,
    color: '#30385f',

    alignSelf: 'center',
      paddingLeft: 0
  },

    startText2: {
      fontSize: 25,
      marginTop: 5,
      color: '#da4383',

      alignSelf: 'center',
        paddingLeft: 0
    },
    liveCoachText: {
      fontSize: 25,

      color: '#30385f',

      alignSelf: 'center',
        paddingLeft: 0
    },

  midContainer: {
    flexDirection: 'column',
    alignItems: 'center',

  },

  backgroundImage: {
      flex: 1,
      resizeMode: 'stretch',
      height:null,
      width:null
       // or 'stretch'
    },

  loginWithFacebookContainer: {

    height: 50,

    flexDirection: 'row',

    borderRadius: 50,
    borderWidth: 0,
   borderColor: 'black',
   backgroundColor: '#407ab1',
    marginTop: 25
  },
  loginContainer: {

    height: 50,

    flexDirection: 'row',

    borderRadius: 50,
    borderWidth: 1,
   borderColor: 'black',
   backgroundColor: '#2a2e56',
    marginTop: 10
  },
  login: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',


    fontSize: 16,
    width: Dimensions.get('window').width*0.8
  },
  forgetpassword: {
    textAlign: 'center',
    color: 'white',
    marginTop: 5,
    fontSize: 15,
    textDecorationLine: 'underline',
    textDecorationColor: 'white',
    paddingBottom: 3
  },
  textInputContainer: {
    height:45,
    width: Dimensions.get('window').width*0.9,
    borderRadius: 2,
    borderWidth: .5,
    borderColor: 'grey',
    backgroundColor:'white',
    fontSize: 14,
    paddingLeft: 8
  },
  indicator:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backContainer: {
    position: 'absolute',
    flexDirection:'row',
    top: 30,
    left: 20,
    alignItems: 'center',
  },
  backButton:{
    color: 'white',
    fontSize: 14,
    marginLeft: 5
  },
  drawerImage:{
    height: 20,
    width: 20,





  },
});

export default styles;
