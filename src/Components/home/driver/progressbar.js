import React, { Component } from 'react';
import {
  StyleSheet,         // CSS-like styles
  Text,               // Renders text
  TouchableOpacity,   // Pressable container
  View    ,Dimensions            // Container component
} from 'react-native';

import StyleDict from '../../../constants/dictStyle'
const {colorSet,fontSet,windowW,windowH}=StyleDict;

export default class Progressbar extends Component {

    render() {
    var props = this.props,
      progressColor =colorSet.blueColor,
      borderColor = colorSet.blueColor,
      backgroundColor = '#777',
      completePerc = props.completePercentage,
    
      incompletePerc = Math.abs(completePerc - 100);
    

    return (

        <View style={styles.containermain}>
      <View style={[styles.container, props.styles]}>
        <View style={[styles.complete, {flex: completePerc,backgroundColor:progressColor}]}></View>
        <View style={[styles.incomplete, {flex: incompletePerc, backgroundColor}]}></View>
      </View>
      <View style={{position: 'absolute',height:20,backgroundColor:'transparent'}}>
      <Text style={{color:colorSet.white}}>{completePerc} % Completed</Text>
      </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({

  container: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colorSet.blueColor,
    justifyContent:'center',
    alignItems:'center',
    flex:1
  },

  containermain: {

    alignItems:'center',
    justifyContent:'center',
    margin:5,
    flex:1,
  },

  complete: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    height:20,
    
   
  },

  incomplete: {
    backgroundColor: "#ffffff",
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
   
    height:20

  }
});

