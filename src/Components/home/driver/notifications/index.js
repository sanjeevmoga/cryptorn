import React, { Component } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  StyleSheet, // CSS-like styles
  Text, // Renders text
  View,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  ScrollView,
  Image

  // Container component
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import {
  Container,
  Header,
  Content,
  Input,
  Item,
  Button as NativeButton,
  Tab,
  Tabs
} from "native-base";
import { Actions as NavigationActions, Actions } from "react-native-router-flux";
import * as authActions from "../../../../actions/authActions.js";
import * as driverActions from "../../../../actions/driverActions.js";
import StyleDict from "../../../../constants/dictStyle";
import constants from "../../../../constants/icons";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import { connect } from "react-redux";
import CoinCard from '../../../common/CoinCard';

class Index extends Component {

  constructor(props) {

    super(props);

    this.state = {
      // First tab is active by default
      activeTab: 0,
      isAllStepsDone: false,
      name: 'new user',
      isLoadingDriver: false,
      payloadAll: '',
      lastPage: 0,
      profilestatus: 'pending',
      activeTab:0
    };

    this.gradient = {
      colors: ["#0B5A8C", `#0C72AC`, `#0DA9F5`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientOrange = {
      colors: ["#FF613A", `#FF882F`, `#FFDD15`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientBlue = {
      colors: ["#4B6EFF", `#3684FF`, `#0DB0FE`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientVoilet = {
      colors: ["#517AEE", `#625DEF`, `#8423F0`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientSky = {
      colors: ["#00A9F5", `#05BDEB`, `#00E5D9`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientHeader = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0.5 },
      end: { x: 1, y: 0 },
      locations: [0.0, 0.95,]

    };

    this.gradientShadow = {
      colors: ["#E0DDED", `#CED0EA`, `#CBD8EC`, `#C8DCEC`],
      start: { x: 0, y: 1 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.4, 0.7, 0.9]
    }

    this._renderHeader = (
      <View style={styles.containerH}>
        {/* <Icon style={[styles.icon,{ color: color }]}
          name={this.props.icon || "circle"}
          size={24} /> */}
  
          <TouchableOpacity onPress={()=>this.navigate() }>
            <Image
              source={constants.hamburger}
              style={{ height: 24, width: 24 }}
            />
          </TouchableOpacity>
      
        <Text style={{fontSize:22,color:'white',fontFamily:'segoeuib'}}>
          EXCHANGE ORDER
        </Text>
       
          <TouchableOpacity>
            {/* <Icon name="bell" size={26} color={colorSet.white} /> */}
          </TouchableOpacity>
       
  
        {/* {badge} */}
      </View>
    );


  }



  componentWillMount() {



  }



  componentWillReceiveProps(newProps) {


  }






  _renderLeftButton = () => {
    return (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => this._showmap()}
      >
        <Icon name="user" size={26} color={colorSet.blueColor} />
      </TouchableOpacity>
    );
  };




  _showmap() {

    NavigationActions.location();

  }

  navigate = () => {
    this.props.navigation.navigate('DrawerOpen');
  }


  _handleIconTouch = () => {

    // NavigationActions.settings();
    Alert.alert(
      "Logout",
      "For Time being it is just for logout,will refer to setting screen in future",
      [
        { text: "Cancel", onPress: () => "" },
        { text: "OK", onPress: () => this.props.logout('logout') }
      ],
      { cancelable: true }
    );
  };

  render() {

    return (
      <View style={styles.container} >
        <View style={styles.containerV}>

          <View style={styles.topLeft}>

            <LinearGradient {...this.gradientHeader} style={{
              marginTop: 0, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <View style={{ height: 160, flex:1,padding:20, justifyContent:'center',alignItems:'center'}}>{this._renderHeader}
             
              </View>
            </LinearGradient>
          </View>
          <LinearGradient {...this.gradientShadow} style={styles.bottomRight}>

          </LinearGradient>
          <LinearGradient {...this.gradientShadow} style={styles.bottomLeft}></LinearGradient>

        </View>
      
          <View style={{ padding: 20,justifyContent:'center',alignItems:'center' }}>
            <View style={{ padding: 20,justifyContent:'center',alignItems:'center',marginTop:windowH/2-200 }}>
            <Image source={constants.ic_order} style={{width:42,height:42}} />
            <Text style={{ marginBottom: 3,marginTop:10, fontWeight: '400', fontSize: 20, color: 'black', }}>No Activity Yet</Text>
            <Text style={{ marginBottom: 12, fontWeight: '400', fontSize: 16, color: 'grey', }}>You don't have any activity yet.</Text>
            <Text style={{ marginBottom: 6, fontWeight: 'bold', fontSize: 18, color: colorSet.blueColor, }}>MAKE A EXCHANGE</Text>
            </View>
          </View>
          
      </View>

    );


  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: "#fff" ,
   // Background color
  },

  containerH: {
    flexDirection: "row",
    height: 30,
    width: windowW,
    alignItems: "center",
    justifyContent: 'space-between',
    padding:10
  },
  // Tab content container
  content: {
    flex: 1,
    margin: 10, // Take up all available space
    backgroundColor: "#fff" // Darker background for content area
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: "#000", // White color
    fontFamily: "Avenir", // Change font family
    fontSize: 26 // Bigger font size
  },
  // Content text
  text: {
    padding: 10, // Add horizontal margin
    // Semi-transparent text
    textAlign: "center", // Center
    fontSize: 18
  },

  percentageContainer: {
    paddingBottom: 10
  },

  containerV: {

    width: windowW,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    marginTop: 0
  },
  topLeft: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: 100,
    width: windowW,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  topRight: {
    position: 'absolute',
    right: 0,
    top: 0
  },
  bottomLeft: {
    position: 'absolute',
    left: 10,
    width: windowW - 20,
    height: 5,
    backgroundColor: '#CDD9ED',
    bottom: 15,
    opacity: 0.5,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20
  },
  bottomRight: {
    position: 'absolute',
    left: 20,
    width: windowW - 40,
    height: 10,
    bottom: 10,
    opacity: 0.5,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    backgroundColor: '#CDD9ED',
  },
  title: {
    flex: 1,
    fontSize: 20,
    color: '#ffffff'
  }

});

const mapDispatchToProps = dispatch => {
  return {
    logout: (response) => {
      authActions.logout(dispatch, response);
    },
    getDriver: () => {
      driverActions.getDriver(dispatch);
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    payloadall: state.driverReducer.payloadAll,
    isLoadingDriver: state.driverReducer.isLoadingdriverdata,
    refresh: state.driverReducer.refresh,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
