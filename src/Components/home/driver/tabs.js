import React, { Component } from 'react';
import {
  StyleSheet,         // CSS-like styles
  Text,               // Renders text
  TouchableOpacity,   // Pressable container
  View,
  ScrollView                // Container component
} from 'react-native';


import StyleDict from '../../../constants/dictStyle'
const {colorSet,fontSet,windowW,windowH}=StyleDict;


export default class Tabs extends Component {


  constructor(props) {
    super(props);
    this.state = {
      // First tab is active by default
      //activeTab:props.activeTab
      activeTab:props.activeTab 
    };
  }

  componentWillReceiveProps(newProps){
    this.setState({activeTab:newProps.activeTab});
  }
      // Initialize State
      // Pull children out of props passed from App component
      render({ children } = this.props) {
        //alert(this.props.activeTab);

        return (
          <View style={styles.container}>
            {/* Tabs row */}

            {/* <ScrollView horizontal={true} > */}
            <View style={styles.tabsContainer}>
              {/* Pull props out of children, and pull title out of props */}
              {children.map(({ props: { title } }, index) =>
                <TouchableOpacity
                  style={[
                    // Default style for every tab
                    styles.tabContainer,
                    // Merge default style with styles.tabContainerActive for active tab
                    index === this.state.activeTab ? styles.tabContainerActive : []
                  ]}
                  // Change active tab
                  onPress={() => this.setState({ activeTab: index }) }
                  // Required key prop for components generated returned by map iterator
                  key={index}
                >
                  <Text style={[styles.tabText,index === this.state.activeTab ? styles.tabTextActive : []]}>
                    {title}
                  </Text>
                  
                </TouchableOpacity>
              )}
            </View>

            {/* </ScrollView> */}
            {/* Content */}
            <View style={styles.contentContainer}>
              {children[this.state.activeTab]}
            </View>
          </View>
        );
      }
    }

    const styles = StyleSheet.create({
        // Component container
        container: {
          flex: 1,                            // Take up all available space
        },
        // Tabs row container
        tabsContainer: {
          flexDirection: 'row',               // Arrange tabs in a row
          padding: 0,                     // Top padding
        },
        // Individual tab container
        tabContainer: {
          flex: 1,                            // Take up equal amount of space for each tab
          paddingVertical: 12,                // Vertical padding
          borderBottomWidth: 0,               // Add thick border at the bottom
          borderBottomColor: 'transparent', 
          backgroundColor: '#d7d7d7'
          
            // Transparent border for inactive tabs
        },
        // Active tab container
        tabContainerActive: {
          backgroundColor: colorSet.blueColor,
                // White bottom border for active tabs
        },
        // Tab text
        tabText: {
          color: '#000',
          fontWeight: '600',
          textAlign: 'center',
          fontSize: 16,
          paddingLeft:2,
          paddingRight:2,
        },

        tabTextActive:{
            color: colorSet.white,
        },
        // Content container
        contentContainer: {
          flex: 1                             // Take up all available space
        }
      });