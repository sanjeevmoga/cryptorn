import React, { Component } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  StyleSheet, // CSS-like styles
  Text, // Renders text
  View,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  ScrollView,
  Image

  // Container component
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import {
  Container,
  Header,
  Content,
  Input,
  Item,
  Button as NativeButton,
  Tab,
  Tabs
} from "native-base";
import { Actions as NavigationActions, Actions } from "react-native-router-flux";
import * as authActions from "../../../actions/authActions.js";
import * as driverActions from "../../../actions/driverActions.js";
import StyleDict from "../../../constants/dictStyle";
import constants from "../../../constants/icons";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import { connect } from "react-redux";
import CoinCard from '../../common/CoinCard';

class App extends Component {

  constructor(props) {

    super(props);

    this.state = {
      // First tab is active by default
      activeTab: 0,
      isAllStepsDone: false,
      name: 'new user',
      isLoadingDriver: false,
      payloadAll: '',
      lastPage: 0,
      profilestatus: 'pending'
    };

    this.gradient = {
      colors: ["#0B5A8C", `#0C72AC`, `#0DA9F5`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientOrange = {
      colors: ["#FF613A", `#FF882F`, `#FFDD15`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientBlue = {
      colors: ["#4B6EFF", `#3684FF`, `#0DB0FE`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientVoilet = {
      colors: ["#517AEE", `#625DEF`, `#8423F0`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientSky = {
      colors: ["#00A9F5", `#05BDEB`, `#00E5D9`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientHeader = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0.5 },
      end: { x: 1, y: 0 },
      locations: [0.0, 0.95,]

    };

    this.gradientShadow = {
      colors: ["#E0DDED", `#CED0EA`, `#CBD8EC`, `#C8DCEC`],
      start: { x: 0, y: 1 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.4, 0.7, 0.9]
    }

    this._renderHeader = (
      <View style={styles.containerH}>
        {/* <Icon style={[styles.icon,{ color: color }]}
          name={this.props.icon || "circle"}
          size={24} /> */}


        <TouchableOpacity onPress={() => this.navigate()} >
          <Image
            source={constants.hamburger}
            style={{ height: 24, width: 24 }}
          />
        </TouchableOpacity>

        <Text style={{ fontSize: 24, color: 'white',fontFamily:"segoeuib" }}>
          My Wallet
        </Text>

        <TouchableOpacity>
          <View>
            <Image
              source={constants.ic_notify}
              style={{ height: 24, width: 24, resizeMode: 'contain' }}
            />
            <View style={{ height:15,width:15, position: 'absolute', top: 0, right: 0, borderRadius: 7.5, backgroundColor:'orange' }}><Text style={{ textAlign: 'center', color: colorSet.white,fontSize:10 }}>{7}</Text></View>
          </View>
        </TouchableOpacity>


        {/* {badge} */}
      </View>
    );

  }



  componentWillMount() {



  }



  componentWillReceiveProps(newProps) {


  }

  navigate = () => {
    this.props.navigation.navigate('DrawerOpen');
  }






  _renderLeftButton = () => {
    return (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => this._showmap()}
      >
        <Icon name="user" size={26} color={colorSet.blueColor} />
      </TouchableOpacity>
    );
  };




  _showmap() {

    NavigationActions.location();

  }


  _handleIconTouch = () => {

    // NavigationActions.settings();
    Alert.alert(
      "Logout",
      "For Time being it is just for logout,will refer to setting screen in future",
      [
        { text: "Cancel", onPress: () => "" },
        { text: "OK", onPress: () => this.props.logout('logout') }
      ],
      { cancelable: true }
    );
  };

  render() {

    return (
      <View style={styles.container} >
        <View style={styles.containerV}>

          <View style={styles.topLeft}>

            <LinearGradient {...this.gradientHeader} style={{
              marginTop: 0, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <View style={{ height: 160, flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center' }}>{this._renderHeader}
                <Text style={{ marginTop: 10, marginBottom: 5, fontWeight: 'bold', fontSize: 14, color: 'white' }}>Available Balance</Text>
                <Text style={{ marginBottom: 6, fontWeight: 'bold', fontSize: 14, color: 'white' }}>0.5689345 BTC</Text>
                <Text style={{ marginBottom: 6, fontWeight: 'bold', fontSize: 32, color: 'white' }}>$9311.11</Text>
              </View>
            </LinearGradient>
          </View>
          <LinearGradient {...this.gradientShadow} style={styles.bottomRight}>

          </LinearGradient>
          <LinearGradient {...this.gradientShadow} style={styles.bottomLeft}></LinearGradient>

        </View>
        <ScrollView>
          <View style={{ paddingTop: 0,paddingBottom:20,paddingLeft:20,paddingRight:20 }}>
            {/* <LinearGradient {...this.gradientHeader} style={{
            marginTop: 20, borderRadius: 6,
            borderWidth: 0,
            borderColor: '#fff'
          }} >
            <View style={{ height: 200, width: 300, }}></View>
          </LinearGradient> */}

            <Text style={{ marginBottom: 0,fontSize: 18, color: 'black',fontFamily:"segoeuib" }}>Protect Your BCH</Text>
            <Text style={{ marginBottom: 6,fontSize: 14, color: 'grey',fontFamily:"segoeui" }}>Due to BCH fork don't send/recieve BCH until further notice</Text>

            <LinearGradient {...this.gradient} style={{
              marginTop: 10, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >

              <CoinCard
                style={{ height: 65, width: windowW - 40 }}
                title="MOBILINK TOKEN"
                subtitle="0 MT"
                price="$0.00"
                icon={constants.icon01}
                color={colorSet.blueColor}
                selectedColor={colorSet.accentColor}
              />

            </LinearGradient>

            <LinearGradient {...this.gradientOrange} style={{
              marginTop: 10, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <CoinCard
                style={{ height: 65, width: windowW - 40 }}
                title="BITCOIN"
                subtitle="0 BTC"
                price="$0.00"
                icon={constants.icon02}
                color={colorSet.blueColor}
                selectedColor={colorSet.accentColor}
              />
            </LinearGradient>

            <LinearGradient {...this.gradientBlue} style={{
              marginTop: 10, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <CoinCard
                style={{ height: 65, width: windowW - 40 }}
                title="ETHERIUM"
                subtitle="0 ETH"
                price="$0.00"
                icon={constants.icon03}
                color={colorSet.blueColor}
                selectedColor={colorSet.accentColor}
              />
            </LinearGradient>

            <LinearGradient {...this.gradientVoilet} style={{
              marginTop: 10, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <CoinCard
                style={{ height: 65, width: windowW - 40 }}
                title="BITCOIN CASH"
                subtitle="0 BCH"
                price="$0.00"
                icon={constants.icon04}
                color={colorSet.blueColor}
                selectedColor={colorSet.accentColor}
              />
            </LinearGradient>

            <LinearGradient {...this.gradientSky} style={{
              marginTop: 10, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <CoinCard
                style={{ height: 65, width: windowW - 40 }}
                title="LITCOIN"
                subtitle="0 LTC"
                price="$0.00"
                icon={constants.icon05}
                color={colorSet.blueColor}
                selectedColor={colorSet.accentColor}
              />
            </LinearGradient>
          </View>
        </ScrollView>

      </View>

    );


  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: "#fff",
    // Background color
  },

  containerH: {
    flexDirection: "row",
    height: 30,
    width: windowW,
    alignItems: "center",
    justifyContent: 'space-between',
    padding: 10
  },
  // Tab content container
  content: {
    flex: 1,
    margin: 10, // Take up all available space
    backgroundColor: "#fff" // Darker background for content area
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: "#000", // White color
    fontFamily: "Avenir", // Change font family
    fontSize: 26 // Bigger font size
  },
  // Content text
  text: {
    padding: 10, // Add horizontal margin
    // Semi-transparent text
    textAlign: "center", // Center
    fontSize: 18
  },

  percentageContainer: {
    paddingBottom: 10
  },

  containerV: {
    width: windowW,
    height: 180,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 0
  },
  topLeft: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: 160,
    width: windowW,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  topRight: {
    position: 'absolute',
    right: 0,
    top: 0
  },
  bottomLeft: {
    position: 'absolute',
    left: 10,
    width: windowW - 20,
    height: 5,
    backgroundColor: '#CDD9ED',
    bottom: 15,
    opacity: 0.5,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20
  },
  bottomRight: {
    position: 'absolute',
    left: 20,
    width: windowW - 40,
    height: 10,
    bottom: 10,
    opacity: 0.5,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    backgroundColor: '#CDD9ED',
  },
  title: {
    flex: 1,
    fontSize: 20,
    color: '#ffffff'
  }

});

const mapDispatchToProps = dispatch => {
  return {
    logout: (response) => {
      authActions.logout(dispatch, response);
    },
    getDriver: () => {
      driverActions.getDriver(dispatch);
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    payloadall: state.driverReducer.payloadAll,
    isLoadingDriver: state.driverReducer.isLoadingdriverdata,
    refresh: state.driverReducer.refresh,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
