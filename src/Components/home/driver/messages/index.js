import React, { Component } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  StyleSheet, // CSS-like styles
  Text, // Renders text
  View,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  ScrollView,
  Image,
  Modal,
  TouchableHighlight,
  paddingTop,
  Keyboard,
  TextInput,
  TouchableWithoutFeedback

  // Container component
} from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { Actions as NavigationActions, Actions } from "react-native-router-flux";
import * as authActions from "../../../../actions/authActions.js";
import * as driverActions from "../../../../actions/driverActions.js";
import StyleDict from "../../../../constants/dictStyle";
import constants from "../../../../constants/icons";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import { connect } from "react-redux";
import HR from '../../../common/hrm';
import ButtonG from '../../../common/ButtonG';
import CoinCard from '../../../common/CoinCard';
import VirtualKeyboard from 'react-native-virtual-keyboard';


class Index extends Component {

  constructor(props) {

    super(props);

    this.state = {
      // First tab is active by default
      activeTab: 0,
      isAllStepsDone: false,
      name: 'new user',
      isLoadingDriver: false,
      payloadAll: '',
      lastPage: 0,
      profilestatus: 'pending',
      modalVisible: false,
      yousend:'0.0',
      youget:'0.0',
      focused:0
    };

    this.gradient = {
      colors: ["#0B5A8C", `#0C72AC`, `#0DA9F5`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientOrange = {
      colors: ["#FF613A", `#FF882F`, `#FFDD15`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientBlue = {
      colors: ["#4B6EFF", `#3684FF`, `#0DB0FE`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };
    this.gradientButton = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
    };

    this.gradientVoilet = {
      colors: ["#517AEE", `#625DEF`, `#8423F0`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientSky = {
      colors: ["#00A9F5", `#05BDEB`, `#00E5D9`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.6, 0.9]
    };

    this.gradientHeader = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0.5 },
      end: { x: 1, y: 0 },
      locations: [0.0, 0.95,]

    };

    this.gradientShadow = {
      colors: ["#E0DDED", `#CED0EA`, `#CBD8EC`, `#C8DCEC`],
      start: { x: 0, y: 1 },
      end: { x: 1, y: 0 },
      locations: [0.1, 0.4, 0.7, 0.9]
    }
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  componentWillMount() {

  }

  componentWillReceiveProps(newProps) {

  }

  changeText=(value)=>{
    if(this.state.focused==0){
      this.setState({yousend:value});
    }else if(this.state.focused==1){
      this.setState({youget:value});
    }
    

  }

  // _renderHeader=() => (
  // );

  _renderLeftButton = () => {
    return (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => this._showmap()}
      >
        <Icon name="user" size={26} color={colorSet.blueColor} />
      </TouchableOpacity>
    );
  };

  _showmap() {
    NavigationActions.location();
  }


  _handleIconTouch = () => {
    // NavigationActions.settings();
    Alert.alert(
      "Logout",
      "For Time being it is just for logout,will refer to setting screen in future",
      [
        { text: "Cancel", onPress: () => "" },
        { text: "OK", onPress: () => this.props.logout('logout') }
      ],
      { cancelable: true }
    );
  };

  navigate = () => {
    this.props.navigation.navigate('DrawerOpen');
  }

  render() {
    let activeTab = this.state.activeTab;

    return (
      <View style={styles.container} >
        <View style={styles.containerV}>

          <View style={styles.topLeft}>

            <LinearGradient {...this.gradientHeader} style={{
              marginTop: 0, borderRadius: 6,
              borderWidth: 0,
              borderColor: '#fff'
            }} >
              <View style={{ height: 160, flex: 1, padding: 30, }}>
                <View style={styles.containerH}>
                  {/* <Icon style={[styles.icon,{ color: color }]}
        name={this.props.icon || "circle"}
        size={24} /> */}

                  <TouchableOpacity onPress={() => this.navigate()}>
                    <Image
                      source={constants.hamburger}
                      style={{ height: 24, width: 24 }}
                    />
                  </TouchableOpacity>
                  <View style={{ flexDirection: 'row', height: 38, borderColor: 'white', borderWidth: 1, borderRadius: 20, width: 200 }}>
                    <TouchableOpacity style={{ flex: 1, height: 36 }} onPress={() => this.setState({ activeTab: 0 })} >
                      <LinearGradient {...this.gradientBlue} style={{ flex: 1, height: 36, borderBottomLeftRadius: 20, borderTopLeftRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white' }}>
                          EXCHANGE
                    </Text>
                      </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flex: 1, height: 36 }} onPress={() => this.setState({ activeTab: 1 })} >
                      <LinearGradient {...this.gradientBlue} style={{ flex: 1, height: 36, borderBottomEndRadius: 20, borderBottomRightRadius: 20, borderTopEndRadius: 20, borderTopRightRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'white' }}>
                          LIMIT ORDER
                    </Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity>
                    {/* <Icon name="bell" size={26} color={colorSet.white} /> */}
                  </TouchableOpacity>
                  {/* {badge} */}
                </View>
              </View>
            </LinearGradient>
          </View>
          <LinearGradient {...this.gradientShadow} style={styles.bottomRight}>
          </LinearGradient>
          <LinearGradient {...this.gradientShadow} style={styles.bottomLeft}></LinearGradient>
          <View style={{
            position: 'absolute', bottom: 0, padding: 0, height: 120, backgroundColor: 'white', borderColor: colorSet.blueColor, borderRadius: 10, borderWidth: 1, flex: 1, alignSelf: 'center', width: windowW - 40
          }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 14, fontWeight: '400', color: colorSet.blueColor, marginLeft: 20, marginTop: 5 }}>
                  {activeTab == 0 ? 'You Send' : 'Amount to send'}
                </Text>
                <TouchableOpacity onPress={()=>{this.setState({focused:0})}}>
                <View>
                <TextInput 
                style={{paddingTop: 0, paddingBottom: 0,fontSize: 14, fontWeight: '400', color: colorSet.greyColor, marginLeft: 10, height:40 }} 
                autoCorrect={false} 
                underlineColorAndroid='transparent' 
                value={this.state.yousend}
                // o={()=>{Keyboard.dismiss(); this.setState({focused:0})}}
               
                editable={false}
                />
                </View>
                </TouchableOpacity>
                 
              </View>
              <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>
                <View style={{ borderTopLeftRadius: 5, borderTopEndRadius: 5, marginTop: 20, marginRight: 10, padding: 5, width: 130, height: 40, backgroundColor: colorSet.blueColor, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <Image
                    source={constants.icon02}
                    style={{ height: 24, width: 24, resizeMode: 'contain', marginRight: 10 }}
                  />
                  <Text style={{ flex: 2, color: 'white' }}>BTC 0</Text>
                  <Image
                    source={constants.up}
                    style={{ height: 24, width: 24, resizeMode: 'contain', marginRight: 10, tintColor: 'white' }}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <HR color={'#f5f5f5'} width={2} opacity={1} />
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 14, fontWeight: '400', color: colorSet.greyColor, marginLeft: 20, marginTop: 5 }}>
                  {activeTab == 0 ? 'You get' : 'Limit amount to send'}
                </Text>
                <TouchableOpacity onPress={()=>{this.setState({focused:1})}} >
                <View>
                <TextInput 
                style={{ paddingTop: 0, paddingBottom: 0,fontSize: 14, fontWeight: '400', backgroundColor:'transparent',color: colorSet.greyColor, marginLeft: 10, height:40 }} 
                autoCorrect={false} 
                underlineColorAndroid='transparent' 
                value={this.state.youget}
                // onFocus={()=>{Keyboard.dismiss(); this.setState({focused:1})}} 
                // keyboardType={'none'}
                editable={false}
                />
                 </View>
                 </TouchableOpacity>
                
                
              </View>
              <TouchableOpacity onPress={() => this.setState({ modalVisible: true })}>

                <View style={{ borderBottomLeftRadius: 5, borderBottomEndRadius: 5, marginRight: 10, padding: 5, width: 130, height: 40, backgroundColor: colorSet.blueColor, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <Image
                    source={constants.icon03}
                    style={{ height: 24, width: 24, resizeMode: 'contain', marginRight: 10 }}
                  />
                  <Text style={{ flex: 2, color: 'white' }}>ETH 0</Text>
                  <Image
                    source={constants.down}
                    style={{ height: 24, width: 24, resizeMode: 'contain', marginRight: 10, tintColor: 'white' }}
                  />
                </View>

              </TouchableOpacity>
            </View>
          </View>

        </View>

        <View style={{ flex: 1, padding: 30 }}>
          {this.state.focused==0 &&  <VirtualKeyboard decimal={true} color='black' pressMode='string'  onPress={(val) => this.changeText(val)} style={{ marginBottom: 30 }} text={this.state.yousend} />}
          {this.state.focused==1 &&  <VirtualKeyboard decimal={true} color='black' pressMode='string'  onPress={(val) => this.changeText(val)} style={{ marginBottom: 30 }} text={this.state.youget} />}
        </View>
        <View style={{ position: 'absolute', bottom: 20, marginLeft: 20, marginRight: 20, width: windowW - 40 }}>
          <ButtonG gradient={this.gradientButton} text={activeTab == 0 ? 'EXCHANGE' : 'SET LIMIT PRICE'} />
        </View>

        {this.state.modalVisible && <Modal
          transparent={true}
          visible={this.state.modalVisible}
          animationType="slide"
          onRequestClose={() => this.setModalVisible(!this.state.modalVisible)}
        >

          <TouchableHighlight style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#ffffff',
          }}
            onPress={() => this.setModalVisible(!this.state.modalVisible)}

          >
            <View style={{
              width: windowW - 60,
              height: 380,
              backgroundColor: 'white',
              borderRadius: 6

            }}>

              <LinearGradient {...this.gradient} style={{
                marginTop: 10, borderRadius: 6,
                borderWidth: 0,
                borderColor: '#fff'
              }} >

                <CoinCard
                  style={{ height: 65, width: windowW - 40 }}
                  title="MOBILINK TOKEN"
                  subtitle="0 MT"
                  price="$0.00"
                  icon={constants.icon01}
                  color={colorSet.blueColor}
                  selectedColor={colorSet.accentColor}
                />

              </LinearGradient>

              <LinearGradient {...this.gradientOrange} style={{
                marginTop: 10, borderRadius: 6,
                borderWidth: 0,
                borderColor: '#fff'
              }} >
                <CoinCard
                  style={{ height: 65, width: windowW - 40 }}
                  title="BITCOIN"
                  subtitle="0 BTC"
                  price="$0.00"
                  icon={constants.icon02}
                  color={colorSet.blueColor}
                  selectedColor={colorSet.accentColor}
                />
              </LinearGradient>

              <LinearGradient {...this.gradientBlue} style={{
                marginTop: 10, borderRadius: 6,
                borderWidth: 0,
                borderColor: '#fff'
              }} >
                <CoinCard
                  style={{ height: 65, width: windowW - 40 }}
                  title="ETHERIUM"
                  subtitle="0 ETH"
                  price="$0.00"
                  icon={constants.icon03}
                  color={colorSet.blueColor}
                  selectedColor={colorSet.accentColor}
                />
              </LinearGradient>

              <LinearGradient {...this.gradientVoilet} style={{
                marginTop: 10, borderRadius: 6,
                borderWidth: 0,
                borderColor: '#fff'
              }} >
                <CoinCard
                  style={{ height: 65, width: windowW - 40 }}
                  title="BITCOIN CASH"
                  subtitle="0 BCH"
                  price="$0.00"
                  icon={constants.icon04}
                  color={colorSet.blueColor}
                  selectedColor={colorSet.accentColor}
                />
              </LinearGradient>

              <LinearGradient {...this.gradientSky} style={{
                marginTop: 10, borderRadius: 6,
                borderWidth: 0,
                borderColor: '#fff'
              }} >
                <CoinCard
                  style={{ height: 65, width: windowW - 40 }}
                  title="LITCOIN"
                  subtitle="0 LTC"
                  price="$0.00"
                  icon={constants.icon05}
                  color={colorSet.blueColor}
                  selectedColor={colorSet.accentColor}
                />
              </LinearGradient>
            </View>
          </TouchableHighlight>
        </Modal>
        }

      </View>
    );

  }
}

const styles = StyleSheet.create({
  // App container
  container: {
    flex: 1, // Take up all screen
    backgroundColor: "#fff",
    // Background color
  },

  containerH: {
    flexDirection: "row",
    height: 30,
    width: windowW,
    alignItems: "center",
    justifyContent: 'space-between',
    padding: 10
  },
  // Tab content container
  content: {
    flex: 1,
    margin: 10, // Take up all available space
    backgroundColor: "#fff" // Darker background for content area
  },
  // Content header
  header: {
    margin: 10, // Add margin
    color: "#000", // White color
    fontFamily: "Avenir", // Change font family
    fontSize: 26 // Bigger font size
  },
  // Content text
  text: {
    padding: 10, // Add horizontal margin
    // Semi-transparent text
    textAlign: "center", // Center
    fontSize: 18
  },

  percentageContainer: {
    paddingBottom: 10
  },

  containerV: {

    width: windowW,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    marginTop: 0
  },
  topLeft: {
    position: 'absolute',
    left: 0,
    top: 0,
    height: 180,
    width: windowW,
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  topRight: {
    position: 'absolute',
    right: 0,
    top: 0
  },
  bottomLeft: {
    position: 'absolute',
    left: 10,
    width: windowW - 20,
    height: 5,
    backgroundColor: '#CDD9ED',
    bottom: 15,
    opacity: 0.5,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20
  },
  bottomRight: {
    position: 'absolute',
    left: 20,
    width: windowW - 40,
    height: 10,
    bottom: 10,
    opacity: 0.5,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    backgroundColor: '#CDD9ED',
  },
  title: {
    flex: 1,
    fontSize: 20,
    color: '#ffffff'
  }

});

const mapDispatchToProps = dispatch => {
  return {
    logout: (response) => {
      authActions.logout(dispatch, response);
    },
    getDriver: () => {
      driverActions.getDriver(dispatch);
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    payloadall: state.driverReducer.payloadAll,
    isLoadingDriver: state.driverReducer.isLoadingdriverdata,
    refresh: state.driverReducer.refresh,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
