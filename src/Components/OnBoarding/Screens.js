import React, { Component, } from 'react'
import {
  StyleSheet,   // CSS-like styles
  Text,         // Renders text
  View,
  Image, Dimensions        // Container component
} from 'react-native';


import Icon from 'react-native-vector-icons/Ionicons';
import Button from './Button';
import Swiper from './Swiper';
import Contants from '../../constants/icons'

import StyleDict from "../../constants/dictStyle";

const { colorSet, fontSet, windowW, windowH } = StyleDict;

import LinearGradient from 'react-native-linear-gradient';


const remote = 'https://s15.postimg.org/tw2qkvmcb/400px.png';

import constants from '../../constants/icons'




class Screens extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {
      isDriver: props.isDriver

    }
    this. viewpage=( <View style={[styles.slide, { backgroundColor:'#ffffff',alignSelf:'center' }]}>
    <Image source={Contants.bluelogo} style={{resizeMode:'contain',width:80,height:80,marginBottom:25}} />
    <Text style={{ fontSize: 24, color: 'black' ,fontFamily:"segoeui"  }} >Welcome to</Text>
    <Text style={{marginTop:-20}}>
      <Text style={{ fontSize: 48, color: 'black',fontFamily:"segoeuib" }} >ZENUX</Text>
      <Text style={{ fontSize: 36, color: 'black',padding:10,fontFamily:"segoeui"}} >wallet</Text>
    </Text>
    <Text style={{fontSize:18,color:'grey',fontFamily:"segoeuil",marginTop:0}}> The easiest way to crypto</Text>
    </View>);
  }
  
  render() {
    const resizeMode = 'cover';
    const text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry';


    return (
      <Swiper isDriver={this.state.isDriver}>
          {this.viewpage}
          {this.viewpage}
          {this.viewpage}
      </Swiper>
    )
  }
}

const styles = StyleSheet.create({
  // Slide styles
  slide: {
    flex: 1,                    // Take up all scree 
    backgroundColor: 'transparent',
    justifyContent: 'center',
    padding: 30
    // alignItems: 'center'
    // Center horizontally
  },
  contentLayout: {

    flexDirection: 'column',
    position: "absolute",
    backgroundColor: 'transparent'
  },
  // Header styles
  header: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 15,

  },
  // Text below header
  text: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: 18,
    marginVertical: 20,
    textAlign: 'center',

  },
});

const iconStyles = {
  size: 100,
  color: '#FFFFFF',
};

export default Screens