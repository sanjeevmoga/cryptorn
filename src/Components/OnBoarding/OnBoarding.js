import React, { Component, } from 'react'
import { View, StatusBar, StyleSheet, Text, Dimensions,TouchableOpacity } from 'react-native'
import Screens from './Screens';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
const { width, height } = Dimensions.get('window');
import ButtonG from '../common/ButtonG';


class OnBoarding extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
    this.onFinish = this.onFinish.bind(this)

    this.gradientBlue = {
      // colors: ["#4B6EFF", `#3684FF`, `#0DB0FE`],
     
      // locations: [0.1, 0.6, 0.9]

      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
      //locations: [0.0, 0.95,]
    };
  }

  onFinish() {
    //alert('here');
  }

  componentDidMount() {
    // Hide the status bar
    StatusBar.setHidden(true);
  }

  navigate(){
   
    Actions.createwallet();
  }

  render() {
    let { title, isDriver } = this.props;
    return (
      <View style={{ width: width, height: height ,backgroundColor:'#ffffff'}}>
        <View style={{ width: width, height: height*0.70,backgroundColor:'#ffffff' }}>
          <Screens />
        </View>
        <View style={{ flex:1, height: height*0.30,paddingLeft:30,paddingRight:30,paddingBottom:30,backgroundColor:'#fff',justifyContent:'center' }}>
        <ButtonG gradient={this.gradientBlue} text={'Continue'}  onPress={this.navigate} />
        <TouchableOpacity onPress={()=> Actions.drawer({type:'reset'}) }>
        <Text style={{ fontSize:16,color:'black',marginTop:15,alignSelf:'center'}}><Text>or</Text> <Text style={{fontWeight:'bold',textDecorationLine:'underline',textDecorationColor:'green'}}>restore</Text> an existing wallet</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default OnBoarding;

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,

  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});