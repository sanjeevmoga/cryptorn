
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import DictStyle from '../../constants/dictStyle';
import Strings from '../../constants/strings';

export default class CategoryHeader extends Component<{}> {
	static propTypes = {
		coachingType: PropTypes.number,
	};

  static defaultProps = {
    coachingType: 0,
  };

	constructor(props) {
		super(props);
    this.state = {
      data: props.coachingType
    };
	}

  onTabPressed = (coaching) => {
    let coachingValue = this.state.data;
    if (coaching === 0) { //clicked on personal coaching
        coachingValue = 0;
    } else if (coaching === 1) { // clicked on professional coaching
      coachingValue = 1;
    }
    this.setState({ data: coachingValue });
  }

	render() {
		const { data } = this.state;
		return (
			<View style={styles.tabsContainer}>
        <TouchableOpacity
          style={styles.tabTouch}
          onPress={() => this.onTabPressed(0)}
        >
          <Text
            style={data === 0 ? styles.tabSelected : styles.tabUnSelected}
          >
            {Strings.personalCoaching}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabTouch}
          onPress={() => this.onTabPressed(1)}
        >
          <Text
            style={data === 1 ? styles.tabSelected : styles.tabUnSelected}
          >
            {Strings.profesionalCoaching}
          </Text>
        </TouchableOpacity>
      </View>
		);
	}
}

const styles = StyleSheet.create({
  tabsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTouch: {
    flex: 1
  },
  tabSelected: {
    textAlign: 'center',
    color: DictStyle.colorSet.mainText,
    borderColor: DictStyle.colorSet.mainText,
    borderBottomWidth: 2,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 10,
  },
  tabUnSelected: {
    textAlign: 'center',
    color: DictStyle.colorSet.lightText,
    borderColor: DictStyle.colorSet.lightText,
    borderBottomWidth: 2,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 10,
  },
});
