import React from 'react';
import { View } from 'react-native';

const HR = (props) => {
    return (
        <View
        style={{
          borderBottomColor: props.color,
          borderBottomWidth: 0.5,
          opacity:0.3,
          marginTop:5
        }}
      />
    );
};



export default HR;
