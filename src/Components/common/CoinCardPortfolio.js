/**
 * Copyright 2016 Facebook, Inc.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * Facebook.
 *
 * As with any software that integrates with the Facebook platform, your use
 * of this software is subject to the Facebook Developer Principles and
 * Policies [http://developers.facebook.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE
 */

"use strict";

import F8Colors from "./F8Colors";

import { Text } from "./F8Text";
import F8Touchable from "./F8Touchable";
import React, { Component } from 'react';
import { StyleSheet, Image, View } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IconsConstant from '../../constants/icons';
import LinearGradient from 'react-native-linear-gradient';

import HR from './hr';

class MenuItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    //let icon = this.props.selected ? this.props.selectedIcon : this.props.icon;
    let icon = this.props.selected;
    const selectedTitleStyle = this.props.selected && styles.selectedTitle;
    let color = this.props.selected ? this.props.selectedColor : this.props.color;
    let badge;
    let subtitle;
    let price;
    let inflation;
    if (this.props.subtitle) {
      subtitle = (
        <Text style={[styles.subtitle, selectedTitleStyle]}>
          {this.props.subtitle}
        </Text>
      );
    }

    if(this.props.inflation){
      inflation = (
        <Text style={[styles.subtitle, selectedTitleStyle]}>
          {this.props.inflation}
        </Text>
      );
    }

    if (this.props.price) {
      price = (
        <View style={{backgroundColor:'transparent'}}>
          <Text style={[styles.subtitle,{fontWeight:'600'}]}>
            {this.props.price}
          </Text>
        </View>
      );
    }
    badge = (
      <View>
        <Image
          source={IconsConstant.rightArrow} />
      </View>
    );

    return (
      <F8Touchable onPress={this.props.onPress}>

        <View style={styles.container}>
        <Image source={this.props.icon}
             style={{width:48,height:48,margin:5 }}
            
          />
          <View style={{ flex: 1, flexDirection: 'column',justifyContent:'center' }}>
            <Text style={[styles.title, selectedTitleStyle]}>
              {this.props.title}
            </Text>
            {subtitle}
          </View>
          {price}
          <View style={{width:15}}></View>
          <LinearGradient {...this.props.gradient} style={{
              marginTop: 0, borderBottomRightRadius: 6,
              borderTopRightRadius:6,
              borderWidth: 0,
              borderColor: '#fff',width:70,
              paddingBottom:10
            }} >
            <View style={{flex:1, alignItems: "center",
              justifyContent:'flex-end'
              }}>
              <View style={{flexDirection:'row'}}>{inflation}
              <Image source={IconsConstant.up} style={{width:24,height:24,tintColor:'white'}} />
              </View>
              </View>
            </LinearGradient>
        </View>
      </F8Touchable>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    justifyContent:'center'

  },

  icon: {
    marginRight: 20,
    marginLeft: 20
  },
  title: {
    fontSize: 14,
    color: F8Colors.white,
    fontFamily:"seguisb",
  },
  subtitle: {
    fontSize: 14,
    color: F8Colors.white
  },
  selectedTitle: {
    color: F8Colors.darkText
  },
  badge: {
    backgroundColor: "#DC3883",
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderRadius: 10
  },
  badgeText: {
    fontSize: 12,
    color: "white"
  }
});

module.exports = MenuItem;
