import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet,Text } from 'react-native';
import Item from './Item.js';


class Row extends Component {


  constructor(props) {
		super(props);
    this.state = {
      isSelected:false,
      selectionids:[]
    };
    this.updateSelection=this.updateSelection.bind(this);

  }

   alertItemName = (item, index) => {

    
      //this.props.onClick(index);
   }

   updateSelection(id){

    if(this.state.selectionids.indexOf(id)==-1 ){

      this.state.selectionids.push(id);

    }else{

      var index = this.state.selectionids.indexOf(id);
      if (index > -1) {
        this.state.selectionids.splice(index, 1);
    }

    }

     alert(this.state.selectionids);
   }


   render() {
      const data = this.props.dataSource;
      console.log(this.state.selectionids);
      return (
         <View style={{ flex: 1 }}>
            {
               data.map(value => (
                 <Item item={value}  updateSelection={this.updateSelection}  selectionids={this.state.selectionids}/>
               ))
            }
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
   },
   text: {
    color: '#4f603c'
   },
   outercircle: {

    width: 40,
    height: 40,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DEE1E9',
    borderWidth: 1,
    overflow: 'hidden'


   },
   innerimage: {

    width: 38,
    height: 38,
    borderRadius: 20,
    borderColor: '#f3f4f7',
    borderWidth: 2,
    overflow: 'hidden'

   },
    selectedcircle: {
    width: 40,
    height: 40,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DB4383',
    borderWidth: 1,
    overflow: 'hidden'


   },
});
export default Row;
