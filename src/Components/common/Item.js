import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet,Text } from 'react-native';



class Item extends Component {

   alertItemName = (item) => {

    this.setState({isSelected:!this.state.isSelected})

    this.props.updateSelection(item);
  
  }


      //this.props.onClick(index);
   

   constructor(props) {
    super(props);
    this.state={
      isSelected:false

    }
   // alert(JSON.stringify(props));
    
  }

   render() {
      const item = this.props.item;
     
     // alert(JSON.stringify(this.props.selectionids));
      
         if(this.props.selectionids.indexOf(this.props.item.coaching_category_id)==-1 ){
          
                 sel=false;
          
              }else{
                
                sel=true;
               
              }
          
          this.state = {
      
            isSelected:sel
      
          };
      return (
         <View style={{ flex: 1 }}>
                  <TouchableOpacity
                     onPress={() => this.alertItemName(item.coaching_category_id)}
                  >
                   <View style={styles.container}>
                   <Text style={{ flex: 1,alignSelf:'center' }}>
                    {item.name}
                  </Text>
                        <View style={this.state.isSelected ? styles.selectedcircle : styles.outercircle} >
                        <Image style={styles.innerimage} source={{uri:'https://d30y9cdsu7xlg0.cloudfront.net/png/6156-200.png'}} />
                    </View>
                    </View>
                  </TouchableOpacity>
            
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
   },
   text: {
    color: '#4f603c'
   },
   outercircle: {

    width: 40,
    height: 40,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DEE1E9',
    borderWidth: 1,
    overflow: 'hidden'


   },
   innerimage: {

    width: 38,
    height: 38,
    borderRadius: 20,
    borderColor: '#f3f4f7',
    borderWidth: 2,
    overflow: 'hidden'

   },
    selectedcircle: {
    width: 40,
    height: 40,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DB4383',
    borderWidth: 1,
    overflow: 'hidden'


   },
});
export default Item;
