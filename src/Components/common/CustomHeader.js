import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, ActivityIndicator, Image, ScrollView,TouchableOpacity,Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import constants from "../../constants/icons";
export default class CustomHeader extends Component{
    constructor(props)
    {
       super(props);
    }

    render(){
        return(<View style={{ height: 70, backgroundColor: 'transparent', flexDirection: 'row', width:width,alignItems:'center' }}>

        <TouchableOpacity style={{ flex: 1,marginLeft:20,alignSelf:'center' }} onPress={this.props.onBackPress}>
            <Image source={constants.ic_back_arrow} style={{width:24,height:24,resizeMode:'contain'}} />
        </TouchableOpacity>
        <View style={{ flex: 1 ,alignSelf:'center' }}>
        <Image source={constants.ic_logo_with_text} style={{width:100,height:45,resizeMode:'contain'}} />

        </View>
        <View style={{ flex: 1 }}>

        </View>

      </View>);
    }
}