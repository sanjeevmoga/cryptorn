/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Card,
  DeckSwiper } from 'native-base';

import {
  Dimensions,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity
   } from 'react-native';
import Row from '../../common/Row';
import Userdetail from '../../common/Userdetail';
import Icons from '../../constants/icons';
import MockData from '../../constants/mockData';

let { width: screenWidth } = Dimensions.get('window');

export default class App extends Component<{}> {

  constructor(props) {
    super(props);
    screenWidth = screenWidth - 20 - 60;
  }

  onRowImageClicked = (index) => {
    const minX = 0;
    const defaultRowWidht = 62;
    const widths = MockData.data.length * defaultRowWidht;

    // Maximum possible X position value to prevent scrolling after the last day
    const maxX = (widths > screenWidth) ?
                    (widths - screenWidth) :
                    0; // no scrolling if there's nowhere to scroll

    let scrollToX;
    scrollToX = (defaultRowWidht * (index + 1))
      - (screenWidth / 2) - (defaultRowWidht / 2);

    // Do not scroll over the left edge
    if (scrollToX < minX) {
      scrollToX = 0;
    }

    // Do not scroll over the right edge
    else if (scrollToX > maxX) {
      scrollToX = maxX;
    }

    this.scrollView.scrollTo({ x: scrollToX });
  }

  scrollView;

  leftscrollimage = () => {
    alert('Left image scroll called');
  }

  rightscrollimage = () => {
    alert('Right image scroll called');
  }

  swipetoRight = () => {
    alert('Right swipe');
  }

  swipetoLeft = () => {
    alert('Left Swipe');
  }

  addskippedlist = () => {
      alert('Added to skipped list');
    }

  addfavouritelist = () => {
    alert('Added to favourite list');
  }

  render() {
    const data = MockData.data;
    return (
        <View style={styles.container}>
          <View style={styles.cardcontainer}>
            <TouchableOpacity onPress={this.leftscrollimage}>
              <Image
              source={Icons.leftArrow}
              style={styles.imageArrow}
              />
            </TouchableOpacity>

            <ScrollView
              ref={scrollView => { this.scrollView = scrollView; }}
              horizontal
              style={styles.scrollContainer}
              showsHorizontalScrollIndicator={false}
            >
            <Row
              dataSource={data}
              onClick={this.onRowImageClicked}
            />
            </ScrollView>

            <TouchableOpacity onPress={this.rightscrollimage}>
              <Image
                 source={Icons.rightArrow}
                 style={styles.imageArrow}
              />
            </TouchableOpacity>

          </View>

          <View style={styles.deckSwiperStyle} >
            <DeckSwiper
              looping
              onSwipeRight={() => this.swipetoRight()}
              onSwipeLeft={() => this.swipetoLeft()}
              dataSource={data}
              renderItem={item =>
                  <Userdetail
                    data={item}
                  />
              }
            />
          </View>

          <View style={styles.footer}>
            <TouchableOpacity onPress={this.addskippedlist}>
              <Image style={styles.eye_close_image} source={Icons.eye_close} />
            </TouchableOpacity>

            <View style={styles.swipeviewlayout} >
              <TouchableOpacity onPress={this.addskippedlist}>
                <Image style={styles.swipe_view} source={Icons.swipe_icon} />
              </TouchableOpacity>
              <Text style={styles.swipetext}>SWIPE</Text>
              <Text style={styles.texttoswipe}>Left to Skip and Right to Favourite</Text>
            </View>

            <TouchableOpacity onPress={this.addfavouritelist} >
              <Image style={styles.eye_close_image} source={Icons.heart_circle} />
            </TouchableOpacity>
        </View>
        </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column',
  },
  cardcontainer: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    borderColor: '#E0e0e0',
    borderWidth: 0.8,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  deckSwiperStyle: {
    flex: 1,
    backgroundColor: '#fff',
  },
  imageArrow: {
    width: 30,
    height: 30,
  },
  scrollContainer: {
    flex: 1,
  },
  userDetail: {
    flex: 1,
  },
  footer: {
    backgroundColor:'white',
    flexDirection: 'row',
    padding: 10,
  },
  eye_close_image: {
    width: 65,
    height: 65,
    marginLeft: 10,
    marginRight: 10,
  },
  swipe_view: {
      width: 41,
      height: 41,
  },
  swipeviewlayout: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
  },
  swipetext: {
      fontSize: 11,
      color: '#DB4F8A'
  },
texttoswipe: {
      fontSize: 11,
      color: '#2a2f43',
      textAlign: 'center'
  },

});
