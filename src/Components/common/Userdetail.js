import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
	Image,
	ScrollView,
	TouchableOpacity } from 'react-native';
import {
  Thumbnail } from 'native-base';
import Icons from '../../constants/icons';
import StarRating from 'react-native-star-rating';

class Userdetail extends Component <{}> {

	render() {
		const data = this.props.data;
		if(data){


		
		//alert(data);
		const rating = data.rating==null?0:data.rating;

		//console.log(data);

		return (
				<ScrollView
					contentContainterStyle={styles.container}
				>
					<View style={styles.topContainer}>
					<Thumbnail large style={styles.alignthumbnail} source={{ uri: data.avatar }} />
					 <Text style={styles.title} >{data.first_name+" "+data.last_name}</Text> 
					<View style={styles.messagenow} >
						<Image source={Icons.mail} style={styles.imageview} />
						<Text style={styles.messagetitle} >Message</Text>
					</View>

					<View style={styles.averagetime} >
						<Text style={styles.arveragetitle}>Average Response Time:</Text>
						<Text style={styles.time} >2hours</Text>
					</View>

					<Text
						style={styles.profiledescription}
					> 

					{data.about_me}
					</Text>

					<View
						style={styles.startsize}
					>
						<StarRating
							disabled={false}
							emptyStar={Icons.star_gray}
							fullStar={Icons.star}
							halfStar={Icons.star_half}
							maxStars={5}
							rating={rating}
							starSize={20}
						/>
                        <Text style={styles.recommendtext} >
							'"Bryan is profession coach  would recommend him!"'
						</Text>
					</View>
</View>
				</ScrollView>

		);
	}
	else{

			return null;


}
	}
}
const styles = StyleSheet.create({
	topContainer: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#fff'
	},
	container: {
        flex: 1,
		flexDirection: 'column',
    },
	alignthumbnail: {
		marginTop: 10,
        width: 180,
        height: 180,
        borderRadius: 90,
        alignSelf: 'center'
    },
	title: {
		fontSize: 25,
        fontWeight: 'bold',
        color: '#232728',
        marginTop: 6,
        textAlign: 'center',
        alignSelf: 'center',
    },

    imageview: {
		width: 23,
    height: 23,
    },
    messagenow: {
		flexDirection: 'row',
		justifyContent: 'center',
	},
    messagetitle: {
        fontSize: 15,
        color: '#3c4042',
    },
    averagetime: {
		flexDirection: 'row',
		justifyContent: 'center',
    },
    arveragetitle: {
		fontSize: 12,
		color: '#3c4042'
    },
    time: {
        fontSize: 12,
        color: '#DB4F8A'
    },
    profiledescription: {
        fontSize: 13,
        color: '#7c8183',
        textAlign: 'center',
        padding: 10,
    },
    startsize: {
        padding: 15,
        flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: '#e0e0e0',
		borderBottomWidth: 0.6,

		borderLeftWidth: 0,
		borderRightWidth: 0,
    },
    lineStyle: {
        backgroundColor: '#000000',
        height: 1,
    },
    recommendtext: {
        textAlign: 'center',
        fontStyle: 'italic',
        fontSize: 14,
        width: 180,
        alignSelf: 'center',
    }
});
export default Userdetail;
