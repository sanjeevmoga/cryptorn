
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import DictStyle from '../../constants/dictStyle';
import Icons from '../../constants/icons';

import * as categoryActions from "../../actions/categoryActions.js";
import { connect } from "react-redux";

export default class CategoryItem extends Component<{}> {
	static propTypes = {
		dataSource: PropTypes.object.isRequired,
		headerId: PropTypes.number,
    rowId: PropTypes.number,
	};

	constructor(props) {
		super(props);
    this.state = {
      data: props.dataSource
    };
	}

  rowOnPress = (rowId, sectionId) => {
    const detail = this.state.data;
    detail.checked = !detail.checked;
    this.setState({ data: detail });
    this.props.onCategorySelection(detail.coaching_category_id);
  }

	render() {
		const { headerId, rowId} = this.props;
    const { data } = this.state;
		return (
			<TouchableOpacity key={rowId} onPress={() => this.rowOnPress(rowId, headerId)}>
          <View style={styles.itemContainer}>
            <Text style={styles.item}>{data.name}</Text>
            <Image
              style={styles.itemImage}
              source={data.checked ? Icons.check : Icons.uncheck}
            />
          </View>
        </TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: DictStyle.colorSet.itemBackground,
    padding: 10,
    borderColor: DictStyle.colorSet.headerBorder,
    borderWidth: 1,
   
  },
  item: {
    flex: 1,
    color: DictStyle.colorSet.lightText,
  },
  itemImage: {
    width: 25,
    height: 25
  }
});


