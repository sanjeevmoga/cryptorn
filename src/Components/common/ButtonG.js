import React, { Component, } from 'react'
import {
  StyleSheet,       // CSS-like styles
  Text,             // Renders text
  TouchableOpacity, // Pressable container
  View              // Container component
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class ButtonG extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
  }

  render({ onPress } = this.props ) {
    
    
    
    //alert(JSON.stringify(this.props));
    
    
    return (
     <TouchableOpacity onPress={onPress}>
     
     <LinearGradient {...this.props.gradient} style={styles.button} >
        <View style={{justifyContent:'center',alignItems:'center'}}>
          <Text style={styles.text}>{this.props.text.toUpperCase()}</Text>
        </View>
    </LinearGradient>
    
  </TouchableOpacity>
    )
  }
}


const styles = StyleSheet.create({
  // Button container
  button: {
    borderRadius: 6,         // Rounded border
    borderWidth: 0,           // 2 point border widht
    borderColor: '#FFFFFF',   // White colored border
    paddingHorizontal: 50,    // Horizontal padding
    paddingVertical: 14,     // Vertical padding
  },
  // Button text
  text: {
    color: '#FFFFFF',
    // fontWeight: 'bold',
    fontFamily:'segoeuib'
  },
});

export default ButtonG