import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';


class Row extends Component {

   alertItemName = (item, index) => {
      this.props.onClick(index);
   }

   render() {
      const data = this.props.dataSource;
      return (
         <View style={{ flex: 1, flexDirection: 'row' }}>
            {
               data.map((item, index) => (
                  <TouchableOpacity
                     key={item.id}
                     style={styles.container}
                     onPress={() => this.alertItemName(item, index)}
                  >
                        <View style={this.props.index === index ? styles.selectedcircle : styles.outercircle} >
                        <Image style={styles.innerimage} source={{ uri: item.avatar }} />

                    </View>
                  </TouchableOpacity>
               ))
            }
         </View>
      );
   }
}

const styles = StyleSheet.create({
   container: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
   },
   text: {
    color: '#4f603c'
   },
   outercircle: {

    width: 42,
    height: 42,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DEE1E9',
    borderWidth: 1,
    overflow: 'hidden'


   },
   innerimage: {

    width: 40,
    height: 40,
    borderRadius: 20,
    borderColor: '#f3f4f7',
    borderWidth: 2,
    overflow: 'hidden'

   },
    selectedcircle: {
    width: 42,
    height: 42,
    backgroundColor: 'transparent',
    borderRadius: 21,
    borderColor: '#DB4383',
    borderWidth: 1,
    overflow: 'hidden'


   },
});
export default Row;
