import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Animated,
    Easing,
} from 'react-native';

import StyleDict from '../../constants/dictStyle'
const { colorSet, fontSet, windowW, windowH } = StyleDict;

export default class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            percentage: new Animated.Value(0),
            incompletePercentage: new Animated.Value(100),
            progressText:props.progressText
        };
    }

    componentDidMount() {
        this.update(this.props);
    }

    componentWillReceiveProps(newProps) {
        this.update(newProps);
    }
    update(props) {
        var percentage = props.progress;
        var incompletePercentage = Math.abs(100 - percentage);
        Animated.timing(this.state.percentage, {
            easing: Easing.inOut(Easing.ease),
            duration: 500,
            toValue: percentage
        }).start();
        Animated.timing(this.state.incompletePercentage, {
            easing: Easing.inOut(Easing.ease),
            duration: 500,
            toValue: incompletePercentage
        }).start();
        this.setState({progressText:props.progressText});

    }
    render() {
        var interpolatedPercentage = this.state.percentage.interpolate({
            inputRange: [0, 100],
            outputRange: [0, 100],
        });
        var interpolatedIncompletePercentage = this.state.incompletePercentage.interpolate({
            inputRange: [0, 100],
            outputRange: [0, 100],
        });
        return (
            <View style={[styles.container]}>
                <View style={[styles.progressContainer, this.props.backgroundStyle]}>
                    <Animated.View style={[styles.complete, this.props.progressStyle, { flex: interpolatedPercentage }]}></Animated.View>
                    <Animated.View style={[styles.incomplete, this.props.incompleteStyle, { flex: interpolatedIncompletePercentage }]}></Animated.View>
                </View>
                <View style={styles.textContainer} ><Text style={{textAlign:'center',fontSize:16,fontWeight:'600'}}>{this.state.progressText}</Text></View>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flex: 1,
        alignItems:'center',
        padding:3
    },

    progressContainer: {
        borderRadius: 4,
        borderWidth: 1,
        flexDirection: 'row',
        height: 8,
        marginLeft:15,
        flex:1,
        borderColor: colorSet.blueColor,
    },
    textContainer: {
        paddingLeft:10,
        flex: 1
    },
    complete: {
        marginLeft: 1,
        borderRadius: 4,
        borderWidth: 1,
        marginTop:1,
        marginBottom:1,
        borderColor: colorSet.blueColor
    },
    incomplete: {
        marginRight: 1,
        marginTop:1,
        marginBottom:1,
        borderTopRightRadius: 4,
        borderBottomRightRadius: 4,
        borderWidth: 1,
        borderColor: colorSet.white
    }
});