import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions } from 'react-native';
import { Button } from 'native-base';
import { Image } from 'react-native-animatable';
const WIDTH = Dimensions.get('window').width;
export default class GalleryImage extends Component {
  render() {
    const { uri, index, onPress } = this.props;
    return (
      <Button
        onPress={() => onPress(index)}
        style={{
          backgroundColor: 'transparent',
          borderRadius: 0,
          height: WIDTH / 4,
          width: WIDTH / 4,
          marginLeft:2
        }}
      >
        <Image
          animation={'bounceIn'}
          delay={100 * index}
          duration={500}
          source={uri}
          style={{
            left: 0,
            position: 'absolute',
            resizeMode: 'cover',
            top: 0,
            height: WIDTH / 4,
            width: WIDTH / 4,
            marginLeft:2
          }}
        />
      </Button>
    );
  }
}
GalleryImage.propTypes = {
  index: PropTypes.number,
  onPress: PropTypes.func,
};