
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import DictStyle from '../../constants/dictStyle';
import Icons from '../../constants/icons';

export default class CategoryHeader extends Component<{}> {
	static propTypes = {
		dataSource: PropTypes.object.isRequired,
		headerId: PropTypes.number,
		isExpanded: PropTypes.bool,
	};

	static defaultProps = {
		isExpanded: false,
	};

	constructor(props) {
		super(props);
    this.state = {
      data: props.dataSource,
    };
	}

	render() {
		const { isExpanded, sectionId } = this.props;
    const { data } = this.state;
		return (
			<View style={styles.header}>
            <Image
              style={styles.headerImage}
              source={data.image}
            />

            <Text style={styles.headerTitle}>{data.title}</Text>

            <Image
              style={styles.headerArrow}
              source={isExpanded ? Icons.up : Icons.down}
            />
        </View>
		);
	}
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 1,
    padding: 10,
    borderWidth: 0.8,
    borderColor: DictStyle.colorSet.headerBorder,
  },
  headerTitle: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    color: DictStyle.colorSet.mainText,
    fontWeight: 'bold'
  },
  headerImage: {
    width: 30,
    height: 30
  },
  headerArrow: {
    width: 30,
    height: 25
  }
});
