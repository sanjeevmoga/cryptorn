import React from 'react';
import { View } from 'react-native';

const HR = (props) => {
    return (
        <View
        style={{
          borderBottomColor: props.color,
          borderBottomWidth: props.width,
          opacity:props.opacity
        }}
      />
    );
};



export default HR;
