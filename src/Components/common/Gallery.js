import React, { Component } from 'react';
import { Dimensions, View,ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import ImageViewer from 'ImageViewer';
import GalleryImage from './GalleryImage';
export default class Gallery extends Component {
  constructor(props) {
    super(props);
    this.openLightbox = (index) => {
      this.setState({
        index,
        shown: true,
      });
    };
    this.hideLightbox = () => {
      this.setState({
        index: 0,
        shown: false,
      });
    };
  }
  state = {
    index: 0,
    shown: false,
  };
  render() {
    const { images } = this.props;
    const imagesForPreview=[];
    images.map((item)=>imagesForPreview.push(item.path));

    const { index, shown } = this.state;
    return (
        <ScrollView horizontal={true} >
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}
      >
        {images.map((image, idx) =>
            <GalleryImage
              index={idx}
              key={idx}
              onPress={this.openLightbox}

              uri={{uri:image.path}}


            />
        )}
        <ImageViewer          
          shown={shown}          
          imageUrls={imagesForPreview}          
          onClose={this.hideLightbox}
          index={index}        
        />
      </View>
      </ScrollView>
    );
  }
}
Gallery.propTypes = {
  images: PropTypes.array,
};
