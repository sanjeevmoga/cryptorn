import React, { Component } from 'react';
import { AppRegistry,FlatList,StyleSheet,View,Image} from 'react-native';
import * as driverActions from "../actions/driverActions.js";
import { connect } from "react-redux";
import { Container, Content, Button, Text, H3, List, ListItem, Thumbnail, Body, Card, CardItem, Right } from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome";

import StyleDict from '../constants/dictStyle'
import icons from '../constants/icons'
const { colorSet, fontSet, windowW, windowH } = StyleDict;

class TabIcon extends Component {
    
    constructor(props) {
        super(props);
       // this.ShowHeader = this.ShowHeader.bind(this);
        //this.getViewType = this.getViewType.bind(this);
        this.state = {
          isLoading:false,
          payload:[]
        }
      }

   
    componentWillMount(){
        this.props.notifications();
      }
      componentWillReceiveProps(newProps) {
        
        if(newProps && newProps.payload){
          //alert(JSON.stringify(newProps.payload));
          this.setState({payload:newProps.payload,isLoading:newProps.isLoading});
        }
    
      }

    render() {
      //alert(JSON.stringify(this.props));
      let {payload}=this.state;
      var color = this.props.focused ? colorSet.blueColor : "#9a9a9a";
      var source = this.props.focused?this.props.srcActive:this.props.srcInActive;
      //let showBadage=(payload && payload.data && payload.data.data)?true:false;
      let notiCount=0;
      if(payload && payload.data && payload.data.data){
        notiCount=payload.data.data.length;
      }
  

      return (
        <View
          style={{
            flex: 1
          }}
        >
          <View  style={{
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center"
          }}>
          {/* <Icon
            style={{ color: color }}
            name={this.props.iconName || "circle"}
            size={24}
          /> */}

          <Image source={source}
             style={{width:24,height:24 }}
             size={24}
          />
          <Text
            style={{
              color: color,
              marginTop: 5,
              fontWeight: "400",
              fontSize: 10
            }}
          >
            {this.props.title.toUpperCase()}
          </Text>
          </View>
  
          {/* <View
          style={{
            backgroundColor:colorView,
            flexDirection: "column",
            position:'absolute',
  
           height:10
          }}
        ></View> */}
           {this.props.title==='Notification' &&  notiCount>0 && <View style={{padding:5,position:'absolute',top:0,right:0,borderRadius:20,backgroundColor:colorSet.accentColor}}><Text style={{textAlign:'center',color:colorSet.white}}>{notiCount}</Text></View> }
        </View>
      );
    }
  }
  
 
const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.driverReducer.payloadnoti,
    isLoading: state.driverReducer.isLoadingnoti
  };
};


const mapDispatchToProps = dispatch => {
  return {
    notifications: () => {
      driverActions.notifications(dispatch
      );
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TabIcon);
const styles = StyleSheet.create({
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
   
  },
});