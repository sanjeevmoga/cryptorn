'use strict'

import {
  StyleSheet,
  Dimensions
} from 'react-native';

import StyleDict from '../../constants/dictStyle'
const {colorSet,fontSet,windowW,windowH}=StyleDict;
const { width, height } = Dimensions.get('window');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  bodyContainer: {
    flex: 1,
    alignItems: 'center'

  },
  welcome: {
    marginTop:30,
    height:150,
    width:150

  },
  logintext: {
    fontSize: 20,
    marginTop: 45,
    color: 'black',
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    paddingLeft: 0
  },

    hintloginscreen: {
      fontSize: 16,
      marginTop: 5,
      color: '#2a2e56',
      alignSelf: 'flex-start',
        paddingLeft: 0
    },


  midContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    padding:10
  },

  red:{
    borderBottomColor: 'red',
  },
  loginContainer: {
    height: 50,
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 2,
   borderColor: 'transparent',
   backgroundColor: '#2a2e56',
  
  },

  SignUpContainer: {

    height: 50,

    flexDirection: 'row',

    borderRadius: 50,
    borderWidth: 1,
    marginTop: 5,
    backgroundColor: '#407ab1',

  },


  login: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',
    fontSize: 16,
    width: Dimensions.get('window').width*0.8
  },
  // forgetpassword: {
  //   textAlign: 'center',
  //   color: '#407ab1',
  //   marginTop: 5,
  //   fontSize: 15,
  //   textDecorationLine: 'underline',
  //   paddingBottom: 3
  // },

  error: {
    textAlign: 'left',
    color: '#cc0000',
    marginTop: 5,
    marginLeft:15,
    fontSize: 15
  },
  textInputContainer: {
    height:45,
    width: Dimensions.get('window').width*0.9,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: 'grey',
    backgroundColor:'white',
    fontSize: 14,
    paddingLeft: 8
  },
  indicator:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backContainer: {
    position: 'absolute',
    flexDirection:'row',
    top: 30,

    alignSelf:'flex-start'
  },
  backButton:{
    color: 'white',
    fontSize: 14,
    marginLeft: 5
  },
  backImg:{
    height: 15,
    width: 15
  },

  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
 },
   logo:{
      width: 150,
     height: 150,
     alignSelf:'center'
   },
   logowrapper:{
     flex:1
   },
   text:{
     textAlign:'center',
     fontWeight: '800',
     color:'#fff',
     fontSize:18
   },
   textsignup:{
     textAlign:'center',
     fontWeight: '800',
     color:colorSet.blueColor,
     fontSize:18
   },
   tagline:{
      margin: 15,
      textAlign:'center',
      color:colorSet.blueColor,
      fontSize:22,
      fontWeight:'800'
   },
   forgetpassword:{
     marginHorizontal:5,
     marginTop: 3,
     fontSize:16,
     color:'grey',
     textAlign:'left'
  },
   input: {
     margin: 15,
     height: 40,
     marginBottom: 10,
     borderRadius:0,
     borderBottomWidth:1,
     paddingLeft:1,
     borderBottomColor:colorSet.blueColor,
     backgroundColor:'transparent',
     fontSize:16,
     color:'#000',
  },
  button: {
     backgroundColor: colorSet.blueColor,
     padding: 10,
     margin: 15,
     height: 50,
     borderRadius: 50,         // Rounded border
     borderWidth: 2,           // 2 point border widht
     borderColor: colorSet.blueColor,   // White colored border
     paddingHorizontal: 20,    // Horizontal padding
     paddingVertical: 20,
     justifyContent: 'center',   // Center vertically
     alignItems: 'center',
     
  },
 
  signup: {
   paddingBottom: 20,
     // White colored border
   justifyContent: 'center',   // Center vertically
   alignItems: 'center',
   flexDirection:'row',
   flex:1
   
 },

 imagebackground: {
 
  width:windowW,
  height:windowH,
  resizeMode: 'cover'
 
},

backgroundcontainer: {
  
  width:width,
  height:windowH,
  position:'absolute',
  backgroundColor: 'red',
  padding:10
},

textBoxBtnHolder:
{
  position: 'relative',
  alignSelf: 'stretch',
  justifyContent: 'center'
},

textBox:
{
  fontSize: 18,
  alignSelf: 'stretch',
  height: 45,
  paddingRight: 45,
  paddingLeft: 8,
  borderWidth: 1,
  paddingVertical: 0,
  borderColor: 'grey',
  borderRadius: 5
},

visibilityBtn:
{
  position: 'absolute',
  right: 5,
  height: 30,
  width: 35,
  padding: 5
},

btnImage:
{
  resizeMode: 'contain',
  height: '100%',
  width: '100%'
}
 
 
});

export default styles;
