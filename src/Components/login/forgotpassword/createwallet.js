import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions
} from "react-native";

import { Actions } from 'react-native-router-flux';
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';

import StyleDict from "../../../constants/dictStyle";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import PSIndicator from '../../common/PSIndicator'
import { Container, Header, Content, Input, Item, Button as NativeButton, Picker } from 'native-base';
const { width, height } = Dimensions.get('window');
import ButtonG from '../../common/ButtonG';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import styles from "./styles.js";
import constants from "../../../constants/icons";
import * as authActions from "../../../actions/authActions.js";
import { connect } from 'react-redux';
import HR from '../../common/hrm';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
   

    this.state = {
      password: '',
      confirmpassword: '',
      error: false,
      usernamevalid: false,
      sumbitClicked: false,
      isloading: false,
      payload: '',
      id: props.email,
      otp: props.otp,
      strengthColor: "transparent",
      strengthText: '',
      progress: 0,
      cca2: '',
      text: ''
    };

    this.gradientBlue = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
    };
  }

 

  componentDidMount() {

  }

  componentWillReceiveProps(newProps) {

  }

  validatePasswords() {

    // alert('hello from google');
  }

  showAlert(title, message) {
    Alert.alert(
      title,
      message,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  }

  navigate() {

    Actions.forgot();
  }

  changeText(newText) {
    //this.phone.onChangePhoneNumber(newText);
  }
  render() {
    return (
      <View style={{flex:1,padding:0}}>
        <Image
            resizeMode="contain"
            style={{width:windowW,height:400,resizeMode:'cover'}}
            source={constants.loginbackground}
          />
          
          <View style={[styles.bodyContainer,{padding:30,position:'absolute',top:60,left:10,backgroundColor:'transparent'}]} >
              <Text style={{  fontSize: 36, fontWeight: 'bold', color: 'white', fontFamily:'segoeuib' }} >Hello Avatar</Text>
              <Text style={{  fontSize: 18, fontWeight: '400', color: 'white', marginTop: 10, marginBottom: 30,fontFamily:'segoeui'}} >Create a new wallet or recover your wallet with recovery password or 12-word phrase</Text>
          </View>

        <View style={[styles.bodyContainer,{padding:30}]}>
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            marginBottom: 0
          }}>
            <Text style={{ alignSelf: 'center', fontSize: 18, fontWeight: 'bold', color: 'black', textDecorationLine: 'underline' }} >Recover Wallet</Text>
            <Text style={{ alignSelf: 'center', fontSize: 18, fontWeight: '400', color: 'grey', marginTop: 30, marginBottom: 30 }} >Or</Text>
            <ButtonG style={{ marginTop: 20 }} gradient={this.gradientBlue} text={'Create Wallet'} onPress={this.navigate} />
          </View>
       
      </View>
      </View>
    );
  }
  }


const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.auth.payloadreset,
    isLoading: state.auth.isLoadingreset
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: (password, rpassword, id, otp) => {
      authActions.resetPassword(dispatch, password, rpassword, id, otp);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

