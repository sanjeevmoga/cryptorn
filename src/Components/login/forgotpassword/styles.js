'use strict'

import {
  StyleSheet,
  Dimensions
} from 'react-native';

import StyleDict from '../../../constants/dictStyle'
const {colorSet,fontSet,windowW,windowH}=StyleDict;
const { width, height } = Dimensions.get('window');

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor:'#fff'
  },
  bodyContainer: {
    flex: 1,
    padding:30,
    backgroundColor:'#fff'
  },

  welcome: {
  marginTop: 30,
  height:150,
  width:150,
  alignSelf:'center'
  },

  logintext1: {
    fontSize: 20,
    marginTop: 20,
    color: 'black',
    fontWeight: 'bold',
    alignSelf: 'center',
      paddingLeft: 0
  },
  logintext2: {
    fontSize: 16,
    marginTop: 20,
    color: colorSet.blueColor,
    fontWeight: 'bold',
    alignSelf: 'center',
      paddingLeft: 0
  },
  worrytext: {
    fontSize: 16,
    marginTop: 1,
    width:Dimensions.get('window').width*0.9,
   color:'black',
    alignSelf: 'center',
    alignItems:'center',
      paddingLeft: 0
  },

    hintloginscreen: {
      fontSize: 16,
      marginTop: 5,
      color: '#2a2e56',

      alignSelf: 'flex-start',
        paddingLeft: 0
    },


  midContainer: {
    flexDirection: 'column',
    padding:15,
   
    flex:1

  },
  loginContainer: {

    height: 50,

    flexDirection: 'row',

   borderRadius: 50,
   borderWidth: 2,
   borderColor: 'transparent',
   backgroundColor: colorSet.blueColor,
    marginTop: 25
  },

  SignUpContainer: {
    height: 50,
    flexDirection: 'row',
    borderRadius: 50,
    borderWidth: 1,
    marginTop: 5,
    backgroundColor: '#407ab1',
  },

  error: {
    textAlign: 'left',
    color: '#cc0000',
    marginTop: 5,
    fontSize: 15,
    marginLeft:15
  },


  login: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight:'800',

    width: Dimensions.get('window').width*0.8
  },
  forgetpassword: {
    textAlign: 'center',
    color: 'white',
    marginTop: 5,
    fontSize: 15,
    textDecorationLine: 'underline',
    paddingBottom: 3
  },
  textInputContainer: {
    marginLeft:0,
    height: 40,
    marginBottom: 10,
    borderRadius:0,
    borderBottomWidth:1,
    paddingLeft:1,
    borderBottomColor:colorSet.blueColor,
    backgroundColor:'transparent',
  },

  indicator:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backContainer: {
    position: 'absolute',
    flexDirection:'row',
    top: 30,

    alignSelf:'flex-start'
  },
  backButton:{
    color: 'white',
    fontSize: 14,
    marginLeft: 5
  },
  backImg:{
    height: 15,
    width: 15
  },
  red:{
    borderBottomColor: 'red',
  },
  tagline:{
    margin: 20,
    textAlign:'center',
    color:colorSet.blueColor,
    fontSize:22,
    fontWeight:'800'
 }

});

export default styles;
