import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions
} from "react-native";

import { Actions } from 'react-native-router-flux';
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';
import CustomHeader from "../../common/CustomHeader";

import StyleDict from "../../../constants/dictStyle";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import PSIndicator from '../../common/PSIndicator'
import { Container, Header, Content, Input, Item, Button as NativeButton, Picker } from 'native-base';
const { width, height } = Dimensions.get('window');
import ButtonG from '../../common/ButtonG';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import styles from "./styles.js";
import constants from "../../../constants/icons";
import * as authActions from "../../../actions/authActions.js";
import { connect } from 'react-redux';
import HR from '../../common/hrm';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
   

    this.state = {
      password: '',
      confirmpassword: '',
      error: false,
      usernamevalid: false,
      sumbitClicked: false,
      isloading: false,
      payload: '',
      id: props.email,
      otp: props.otp,
      strengthColor: "transparent",
      strengthText: '',
      progress: 0,
      cca2: '',
      text: ''
    };

    this.gradientBlue = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
    };
  }



  componentDidMount() {

  }

  componentWillReceiveProps(newProps) {

  }

  validatePasswords() {

    // alert('hello from google');
  }

  showAlert(title, message) {
    Alert.alert(
      title,
      message,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  }

  navigate() {

    Actions.resetpasswordconfirm();
  }

  onBackPress(){

    Actions.pop();
  }

  changeText(newText) {
    this.setState({text:newText});
   // this.phone.onChangePhoneNumber(newText);
  }
  render() {
    return (
      <View style={styles.container}>
      <CustomHeader onBackPress={this.onBackPress} />
        <View style={styles.bodyContainer}>
          <View style={{flex:2}}>
          <Text style={{ fontSize: 28, color: 'black', marginTop: 15, fontWeight: 'bold', marginBottom: 5,fontFamily:'segoeuib' }} >Create an Zenux passcode</Text>
            {/* <Text style={{ fontSize: 18, color: 'grey', marginTop: 0, fontWeight: '400', marginBottom: 10 }} >Please enter the code sent to</Text> */}
            <TextInput
            placeholder="Your Passcode"
            underlineColorAndroid="transparent"
            style={[styles.textInputContainer, {marginTop:10}]}
            onChangeText={text => this.validateStrength(text)}
            value={this.state.text}
            editable={false}
          />

          {/* <View style ={{marginTop:10}}>
          <HR  color={colorSet.blueColor} width={1} opacity={1} />
          </View> */}

          </View>
            
            <View style={{flex:1,justifyContent: 'center',paddingBottom:10}}>
            <VirtualKeyboard color='black' pressMode='string' onPress={(val) => this.changeText(val)} style={{marginBottom:30}} />
              </View>
          {/* <TextInput
              placeholder="New Password"
              underlineColorAndroid="transparent"
              style={[styles.textInputContainer, { marginTop: 30 }]}
              onChangeText={text => this.validateStrength(text)}
              value={this.state.password}
              secureTextEntry={true}
            /> */}
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            marginBottom: 0
          }}>
              <ButtonG style={{  marginTop: 20 }} gradient={this.gradientBlue} text={'Next'} onPress={this.navigate} />
          </View>
        </View>
      </View>
    );

  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.auth.payloadreset,
    isLoading: state.auth.isLoadingreset
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: (password, rpassword, id, otp) => {
      authActions.resetPassword(dispatch, password, rpassword, id, otp);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

