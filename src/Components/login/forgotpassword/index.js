import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  Dimensions
} from "react-native";

import { Actions } from 'react-native-router-flux';
import PhoneInput from 'react-native-phone-input';
import CountryPicker from 'react-native-country-picker-modal';

import StyleDict from "../../../constants/dictStyle";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import PSIndicator from '../../common/PSIndicator'
import { Container, Header, Content, Input, Item, Button as NativeButton, Picker } from 'native-base';
const { width, height } = Dimensions.get('window');
import ButtonG from '../../common/ButtonG';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import styles from "./styles.js";
import constants from "../../../constants/icons";
import * as authActions from "../../../actions/authActions.js";
import { connect } from 'react-redux';
import HR from '../../common/hrm';
import CustomHeader from "../../common/CustomHeader";

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.validatePasswords = this.validatePasswords.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);

    this.state = {
      password: '',
      confirmpassword: '',
      error: false,
      usernamevalid: false,
      sumbitClicked: false,
      isloading: false,
      payload: '',
      id: props.email,
      otp: props.otp,
      strengthColor: "transparent",
      strengthText: '',
      progress: 0,
      cca2: '',
      text: ''
    };

    this.gradientBlue = {
      colors: ["#6a47d5", `#33cff8`],
      start: { x: 0, y: 0 },
      end: { x: 1, y: 0 },
    };
  }

  onPressFlag() {
    this.countryPicker.openModal();
  }

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase());
    this.setState({ cca2: country.cca2 });
  }

  componentDidMount() {

  }

  componentWillReceiveProps(newProps) {

  }

  validatePasswords() {

    // alert('hello from google');
  }

  showAlert(title, message) {
    Alert.alert(
      title,
      message,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
  }

  navigate() {

    Actions.otp();
  }

  onBackPress(){

    Actions.pop();
  }

  changeText(newText) {
     this.phone.onChangePhoneNumber(newText);
  }
  render() {
    return (
      <View style={styles.container}>

        <CustomHeader onBackPress={this.onBackPress} />
        <View style={styles.bodyContainer}>


          <View style={{ flex: 2 }}>

            <Text style={{ fontSize: 28, color: 'black', marginTop: 15, fontWeight: 'bold', marginBottom: 5, fontFamily: 'segoeuib' }} >Welcome to Zenux</Text>
            <Text style={{ fontSize: 18, color: 'grey', marginTop: 0, fontWeight: '400', marginBottom: 10, fontFamily: 'segoeuil' }} >Enter your mobile number to continue</Text>
            <PhoneInput
              ref={(ref) => {
                this.phone = ref;
              }}
              textProps={{ placeholder: 'Your Phone Number', placeholderTextColor: '#292929', editable: false }}
              onPressFlag={this.onPressFlag}
              style={{ marginTop: 20 }}
              editable={false}
            //onChangePhoneNumber={(value)=>this.phone.onChangePhoneNumber()}
            // textComponent={()=><TextInput
            //   placeholder="Enter"
            //   underlineColorAndroid="transparent"
            //   style={[styles.textInputContainer, {marginTop:0}]}
            //   onChangeText={text => this.validateStrength(text)}
            //   value={this.state.cca2}
            // />}

            />
            <CountryPicker
              ref={(ref) => {
                this.countryPicker = ref;
              }}
              onChange={value => this.selectCountry(value)}
              translation="eng"
              cca2={this.state.cca2}
            >
              <View />
            </CountryPicker>
            <View style={{ marginTop: 10 }}>
              <HR color={colorSet.blueColor} width={1} opacity={1} />
            </View>

          </View>

          <View style={{ flex: 1, justifyContent: 'center', paddingBottom: 10 }}>
            <VirtualKeyboard backspaceImg={constants.ic_close_round} color='black' pressMode='string' onPress={(val) => this.changeText(val)} style={{ marginBottom: 30 }} />
          </View>
          {/* <TextInput
              placeholder="New Password"
              underlineColorAndroid="transparent"
              style={[styles.textInputContainer, { marginTop: 30 }]}
              onChangeText={text => this.validateStrength(text)}
              value={this.state.password}
              secureTextEntry={true}
            /> */}
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            marginBottom: 0
          }}>
            <ButtonG style={{ marginTop: 20 }} gradient={this.gradientBlue} text={'Next'} onPress={this.navigate} />
          </View>
        </View>
      </View>
    );

  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.auth.payloadreset,
    isLoading: state.auth.isLoadingreset
  };
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: (password, rpassword, id, otp) => {
      authActions.resetPassword(dispatch, password, rpassword, id, otp);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

