import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  ImageBackground,
  AsyncStorage,
  ScrollView,
  ActivityIndicator
} from "react-native";

import { Container, Content } from 'native-base';


import { Actions as NavigationActions } from "react-native-router-flux";
import { Actions } from "react-native-router-flux";
import * as authActions from "../../actions/authActions.js";
import { connect } from "react-redux";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { ListItem, CheckBox, Text as NativeText, Body } from "native-base";

import constants from "../../constants/icons";
//import { authWithPassword, getUserProfile } from '../../helpers/firebase';
//import * as firebase from 'firebase';
//import ActivityIndicator from 'react-native-activity-indicator';

//import { validateEmail } from '../../helpers/validation';
import styles from "./styles.js";

class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      payload: "",
      username: "",
      password: "",
      error: false,
      usernameError: '',
      passwordError: '',
      sumbitClicked: false,
      isChecked: false,
      hidePassword: true,
      isDriver:props.isDriver,
      welcomeText:props.isDriver==true?'Driver Login':'Customer Login'
    };
  }
  componentDidMount() {

    //NavigationActions.tabBar();
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    // SplashScreen.hide();
  }





  userLogin(e) {
    
    Actions.drawer({ type: 'reset',id:null});
    return;

  }

  focusNextField(nextField) {
    this.refs[nextField].focus();
  }

  componentWillReceiveProps(newprops) {
   
  }

  processUserData(data){
   
  }

 

  render() {
      return (

        <Container>
          <Content showsVerticalScrollIndicator={false}>
          
               
                <View style={[styles.backgroundcontainer]}>
                  <View style={styles.logowrapper}>

                    <Text style={styles.tagline}>
                      {this.state.welcomeText}
                    </Text>

                    <TextInput
                      ref="email"
                      placeholder="Email"
                      placeholderTextColor="#000"
                      style={[styles.input]}
                      keyboardType="email-address"
                      onChangeText={(text)=>this.setState({username:text})}
                      returnKeyType="next"
                      value={this.state.username}
                      underlineColorAndroid="transparent"
                      onSubmitEditing={() => this.focusNextField("password")}
                    />

                    <TouchableOpacity>
                      <Text style={styles.error}>{this.state.usernameError}</Text>
                    </TouchableOpacity>

                    <View style={styles.textBoxBtnHolder}>
                      <TextInput
                        ref="password"
                        placeholder="Password"
                        placeholderTextColor="#000"
                        style={[styles.input]}
                        onChangeText={(text)=>this.setState({password:text})}
                        secureTextEntry={this.state.hidePassword}
                        value={this.state.password}
                        underlineColorAndroid="transparent"
                        returnKeyType="done"
                      />

                      <TouchableOpacity activeOpacity={0.8} style={styles.visibilityBtn} onPress={this.managePasswordVisibility}>
                        <Image source={(this.state.hidePassword) ? constants.hide : constants.show} style={styles.btnImage} />
                      </TouchableOpacity>

                    </View>

                    <TouchableOpacity>
                      <Text style={styles.error}>{this.state.passwordError}</Text>
                    </TouchableOpacity>


                    <ListItem style={{ backgroundColor: 'transparent' }}>

                      <CheckBox
                        underlineColorAndroid="transparent"
                        checked={this.state.isChecked}

                        onPress={() =>
                          this.setState({ isChecked: !this.state.isChecked })
                        }
                      />
                      <Body>
                        <TouchableOpacity onPress={() => this.setState({ isChecked: !this.state.isChecked })}>
                          <Text style={[styles.forgetpassword]}>Remember Me</Text>
                        </TouchableOpacity>
                      </Body>

                      <TouchableOpacity onPress={() => NavigationActions.forgot()}>
                        <Text style={styles.forgetpassword}>Forget Password?</Text>
                      </TouchableOpacity>
                    </ListItem>

                    

                    <TouchableOpacity onPress={e => this.userLogin(e)}>
                      <View style={styles.button}>
                        <Text style={styles.text}>LOGIN</Text>
                      </View>
                    </TouchableOpacity>

                    <View style={styles.signup}>
                      <Text style={{ color: 'grey', fontSize: 18, fontWeight: '600' }}>New User ?</Text>
                      <TouchableOpacity onPress={() => NavigationActions.signup({isDriver:this.state.isDriver})}>
                        <NativeText style={styles.textsignup}> Sign Up</NativeText>
                      </TouchableOpacity>
                    </View>
                  </View>
                  </View>
             
             
          </Content>
        </Container>
      );
    }
  }


const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.auth.payload,
    isLoading: state.auth.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (username, password) => {
      authActions.login(dispatch, username, password);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
