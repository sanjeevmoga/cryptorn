'use strict'

import {
  StyleSheet,
  Dimensions,
} from 'react-native';

import StyleDict from '../constants/dictStyle'
const {colorSet,fontSet,windowW,windowH}=StyleDict;

var styles = StyleSheet.create({
  navBar: {
    backgroundColor: '#00A0DC', // changing navbar color
  },
  navTitle: {
    color: 'white', // changing navbar title color
  },
  routerScene: {
    paddingTop: 50, // some navbar padding to avoid content overlap
  },
  leftButton: {
    tintColor: 'red'
  }, tabBar: {
    backgroundColor: "white",
    opacity: 0.98,
    height: 60
  },
  tab: {
    borderColor: "grey",
    borderWidth: 0,
    backgroundColor:colorSet.white,
    flex: 1
  }
});

export default styles;
