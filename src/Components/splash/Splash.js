import React, { Component } from "react";
import { View, Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import constants from "../../constants/icons";
import { AsyncStorage, Dimensions } from "react-native";
import StyleDict from '../../constants/dictStyle'
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get("window");

class Splash extends Component {
  static propTypes = {};
  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {
      showButtons: false
    };
    //alert(this.props.navigation);
  }
  componentWillMount() {
    
  }
  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // AsyncStorage.getItem("userdata").then(data => {
      //       this.processUserData(data);
      // });()
      Actions.onBoarding({type:'reset'});
    }, 500);
  }

  processUserData(data){
    console.log(data);
    if (data){
      let userdata=JSON.parse(data);
      if(userdata.data.role==='DRIVER'){
        if(!this.isMobileVerfied(userdata) && !this.isEmailVerfied(userdata)){
          Actions.phoneotpmatch({type: 'reset',loadnext:true,id:userdata.data._id,userRole:userdata.data.role,phone:userdata.data.phone,email:userdata.data.email,userdata});
          return;
        }
        if(!this.isMobileVerfied(userdata)){
          Actions.phoneotpmatch({type: 'reset',loadnext:false,id:userdata.data._id,userRole:userdata.data.role,phone:userdata.data.phone,email:userdata.data.email,userdata});
          return;
        }
        if(!this.isEmailVerfied(userdata)){
          Actions.emailotpmatch({type: 'reset',loadnext:false,id:userdata.data._id,userRole:userdata.data.role,phone:userdata.data.phone,email:userdata.data.email,userdata});
          return;
        } 
        if(this.isMobileVerfied(userdata) && this.isEmailVerfied(userdata)){
          Actions.drawer({ type: 'reset',id:userdata.data._id});
          return;
        }
       
      }else{
        
        if(!this.isEmailVerfied(userdata)){
          Actions.emailotpmatch({type: 'reset',loadnext:false,id:userdata.data._id,userRole:userdata.data.role,phone:userdata.data.phone,email:userdata.data.email,userdata});
          return;
        } else{
          Actions.drawerCustomer({ type: 'reset',id:userdata.data._id});
        }
        return;
      }
    }else{
      this.setState({ showButtons: true });
    }
  }

  isMobileVerfied(userdata){
    if(userdata && userdata.data &&  userdata.data.phoneVerified){
      if(userdata.data.phoneVerified===1){
          return true;
      }else{
        return false;
      }
    }
  }

  isEmailVerfied(userdata){
    if(userdata && userdata.data &&  userdata.data.emailVerified){
      if(userdata.data.emailVerified===1){
          return true;
      }else{
        return false;
      }
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={constants.splash} style={styles.splash} />
        <View style={[styles.container, { position: "absolute" }]}>
          <Image
            resizeMode="contain"
            source={constants.bluelogo}
            style={[styles.logo]}
          />
          {this.state.showButtons && <View>
            <TouchableOpacity onPress={()=>Actions.login({isDriver:true})}>
            <LinearGradient
              colors={['#115874','#326283' , colorSet.blueColor,colorSet.blueColor, '#326283','#115874']}
              start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
              style={{ height: 48,borderRadius:24, width: 300, alignItems: 'center', justifyContent: 'center', width: 260 }}
            >
              <View style={styles.buttonContainer}>
                <Text style={styles.buttonText}>
                  Take Orders
            </Text>
              </View>
            </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>Actions.login({isDriver:false})} style={{marginTop:10}}>
            <LinearGradient
              colors={['#115874','#326283' , colorSet.blueColor,colorSet.blueColor, '#326283','#115874']}
              start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
              style={{ height: 48,borderRadius:24, alignItems: 'center', justifyContent: 'center', width: 260 }}
            >
              <View style={styles.buttonContainer}>
                <Text style={styles.buttonText}>
                  Place Orders
              </Text>
              </View>
            </LinearGradient>
            </TouchableOpacity>
          </View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // Button container

  // Button text
  container: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },

  splash: {
    width: width,
    height: height
  },
  logo: {
    width: 200,
    height: 200,
    alignSelf: 'center'

  },
  driverButton: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: colorSet.blueColor,
    marginTop: 10,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colorSet.accentColor
  },
  buttonContainer: {
    width: 200,
    alignItems: 'center',
},
buttonText: {
    textAlign: 'center',
    color: colorSet.white,
    fontWeight:'600',
    fontSize:16

}
});

export default Splash;
