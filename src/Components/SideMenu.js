import React, { Component, PropTypes } from 'react';
import { StyleSheet, Text, View, AsyncStorage, ActivityIndicator, Image, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import Button from 'react-native-button';
import { CardSection } from './common/card';
import * as authActions from "../actions/authActions.js";
import { connect } from "react-redux";
import StyleDict from "../constants/dictStyle";
const { colorSet, fontSet, windowW, windowH } = StyleDict;
import MenuItem from "./common/MenuItem"
import { Actions as NavigationActions } from 'react-native-router-flux';

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  container: {
    padding: 5,
    height: 45,
    overflow: 'hidden',
    alignSelf: 'flex-start',
  },
  textStyle: {
    fontSize: 18,
    color: '#555',
  },

  subtitle: {
    paddingHorizontal: 20,
    fontSize: 10,
    color: '#ffffff',
    paddingVertical:6
    
  },
  nameContainer: {
    padding: 15,
    height: 45,
    overflow: 'hidden',
    alignSelf: 'flex-start',
  },
  name: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '400',
    alignSelf: 'center',
  },
  logo: {
    width: 120,
    height: 120,
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 60
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 80
  }
});

class SideMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userdata: {},
      logoutClicked: false,
      deviceToken: '',
      isLoading: false,
      payload: []
    };
  }

  onTabSelect(option) {

    // switch (option) {
    //   case 'logout':
    //     this.props.onLogout();
    //     AsyncStorage.removeItem('userdata')
    //     NavigationActions.start({ type: 'reset' });
    //     break;
    //   case 'scheduler':
    //     Actions.agendascreen();
    //     break;
    //   case 'servingarea':
    //     Actions.location();
    //     break;
    //   case 'home':
    //     Actions.tabs_home();
    //     break;
    //     case 'notifications':
    //     Actions.notification();
    //     break;
    // }
  }

  componentWillReceiveProps(newProps) {
    if (newProps && newProps.payload) {
      this.setState({ payload: newProps.payload, isLoading: newProps.isLoading });
    }
  }

  componentDidMount() {
    AsyncStorage.getItem("userdata").then((data) => {
      if (data)
        this.setState({ userdata: JSON.parse(data).data });
    }).catch(e => console.log(e));

    AsyncStorage.getItem("deviceToken").then((data) => {
      if (data)
        // alert(data);
        // this.setState({ deviceToken: data });
        this.props.updateDeviceToken(data);

    }).catch(e => console.log(e));
  }
  render() {
    if (!this.state.userdata) {
      return null;
    }
    //alert(JSON.stringify(this.state.userdata));
    // return null;
    let data = this.state.userdata;
    //console.log(data);
    let firstName = data.firstName;
    let lastName = data.lastName;
    let email = data.email;
    let phone = data.phone;
    //alert(JSON.stringify(data));
    let uri = 'https://cdn1.iconfinder.com/data/icons/freeline/32/account_friend_human_man_member_person_profile_user_users-256.png';
    try {
      if (data.pic) {
        uri = data.pic;
      }
    }
    catch (e) {
    }
    const { isLoading } = this.props;
    // alert(JSON.stringify(this.state.userdata));
    if (isLoading) {
      return (
        <ActivityIndicator
          animating={isLoading}
          style={styles.activityIndicator}
          size="large"
        />
      );
    } else {

      let { payload } = this.state;
      let notiCount = 0;
      if (payload && payload.data && payload.data.data) {
        notiCount = payload.data.data.length;
      }
      //alert(notiCount);

      return (
       
         <LinearGradient
          colors={[colorSet.blueColor, colorSet.blueColor,colorSet.blueColor,colorSet.blueColor,'#605fdb','#479eeb', '#33cff8','#33cff8','#33cff8' ]}
          style={{ flex: 1, height: windowH }}
          start={{x: 0, y: 1}} end={{x: 1, y: 0}}
        >
         <ScrollView style={[ this.props.sceneStyle]}>
         <View>
            <Text style={[styles.subtitle,{fontSize:20,marginTop:10,fontFamily:"segoeuib"} ]}>
                SETTINGS
            </Text>

             <Text style={[styles.subtitle,{fontSize:32,fontFamily:"segoeuib",marginLeft:3,paddingVertical:5} ]}>
                AVATAR
            </Text>
            <Text style={[styles.subtitle,{marginBottom:30,paddingVertical:0} ]}>
                +1 256 564 5555
            </Text>

            <Text style={styles.subtitle }>
                BACKUP
            </Text>
            <MenuItem
              title="Wallet Backup"
              subtitle="Home"
              selected={this.props.activeTab === 'home'}
              onPress={this.onTabSelect.bind(this, 'home')}
              icon={'home'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}

            />
             <Text style={styles.subtitle }>
             NOTIFICATIONS
            </Text>

            <MenuItem
              title="Notifications"
              selected={this.props.activeTab === 'scheduler'}
              onPress={this.onTabSelect.bind(this, 'scheduler')}
              icon={'clock-o'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            //badge={3}
            />
              <Text style={styles.subtitle }>
             ACCOUNT
            </Text>
            {/* <MenuItem
              title="Native Currency"
              selected={this.props.activeTab === 'servingarea'}
              onPress={this.onTabSelect.bind(this, 'servingarea')}
              icon={'globe'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            /> */}

            <MenuItem
              title="Exchange Limits"
              selected={this.props.activeTab === 'servingarea'}
              onPress={this.onTabSelect.bind(this, 'servingarea')}
              icon={'globe'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            />
            {/* <MenuItem
              title="Tools"
              selected={this.props.activeTab === 'servingarea'}
              onPress={this.onTabSelect.bind(this, 'servingarea')}
              icon={'globe'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            /> */}
           

              <Text style={styles.subtitle }>
             SECURITY
            </Text>

             <MenuItem
              title="Passcode"
              selected={this.props.activeTab === 'servingarea'}
              onPress={this.onTabSelect.bind(this, 'servingarea')}
              icon={'globe'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            />
            {/* <MenuItem
              title="Support"
              selected={this.props.activeTab === 'servingarea'}
              onPress={this.onTabSelect.bind(this, 'servingarea')}
              icon={'globe'}
              color={colorSet.blueColor}
              selectedColor={colorSet.accentColor}
            /> */}

             <Text style={styles.subtitle }>
             App Version 1.4.5
            </Text>
             </View>
            </ScrollView>
            </LinearGradient>
       
       
      );
    }
  }
}




const mapStateToProps = (state, ownProps) => {
  return {
    payload: state.driverReducer.payloadnoti,
    isLoading: state.driverReducer.isLoadingnoti
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => {
      authActions.logout(dispatch, true);
    },
    updateDeviceToken: (deviceToken) => {
      authActions.updateDeviceToken(dispatch, deviceToken);
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
