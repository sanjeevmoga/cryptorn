import api from '../constants/api';
import instance from './config.js';
import * as types from './actionTypes';
const queryString = require('qs');
 
export const clientCategory = (dispatch) => {
    dispatch({
        type: types.CLIENT_CATEGORY,
        isLoading:true,
        payload :''
    });

    return  instance.get(api.CATEGORIES)
            .then((responseJson) => {
            return dispatch({
            type: types.CLIENT_CATEGORY,
            isLoading:false,
            payload :responseJson.data.data
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
};


export const setClientGoals = (dispatch,need_help_with,coaching_categories) => {

  //  alert(coaching_categories);
    dispatch({
        type: types.GOALSSETTING,
        inprogress:true,
        goalsresponse :''
    });

    return  instance.post(api.ACCOUNT, queryString.stringify({
        need_help_with,
        coaching_categories
      }))
   
    .then((responseJson) => {
        return dispatch({
            type: types.GOALSSETTING,
            inprogress:false,
            goalsresponse :responseJson
            
        });
    })
    .catch((error) => {
       // alert(error);
      console.log(JSON.stringify(error));
    });
};

export const setOpenedSections=(dispatch,openedSectionsIds)=>{
        
    return dispatch({
        type: types.SETOPENEDSECTIONS,
        openedSectionsIds:openedSectionsIds,

    });


}


export const setSelectedCategories=(dispatch,cat_id)=>{
    
return dispatch({
    type: types.SETSELECTEDCATEGORIES,
    cat_id:cat_id,
});
}



 
