import api from '../constants/api';
import instance from './config.js';
const queryString = require('qs');
import * as types from '../actions/actionTypes';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

export const personalinfo = (dispatch, name, dob, gender, ssn, profilePic, address, about, page, role, jobexperience,
    experiencetype,firstName,lastName,licenseNo) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.SETPERSONALINFO,
                isLoadingPersonal: true,
                payloadPersonal: ''
            });

            let fromData = new FormData();
            fromData.append("phone", name);
            fromData.append("dob", dob);
            fromData.append("gender", gender);
            fromData.append("ssn", ssn);
            //alert(JSON.stringify(profilePic));
            if(profilePic.uri){
                var extension = profilePic.type.substring(profilePic.type.lastIndexOf('/') + 1, profilePic.type.length);
                fromData.append('profilePic', {
                    uri: profilePic.uri,
                    name: new Date().getTime()+"."+extension,
                    type: profilePic.type
                });
            }
            fromData.append("role", role);
            fromData.append("page", page);
            fromData.append("experienceMon", jobexperience);
            fromData.append("experienceType", experiencetype);
            fromData.append("address", address);
            fromData.append("about", about);
            fromData.append("firstName", firstName);
            fromData.append("lastName", lastName);
            fromData.append("licenseNo", licenseNo);
          
            var config = {
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    console.log("Progress:-" + percentCompleted);
                }
            };

            return instancenew.post(api.SETPERSONALINFO, fromData, config)

                .then((responseJson) => {

                    //alert(JSON.stringify(responseJson));
                    return dispatch({
                        type: types.SETPERSONALINFO,
                        isLoadingPersonal: false,
                        payloadPersonal: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};


export const driverinfo = (dispatch,plateNo, type, make, model, location, page, role) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });
            dispatch({
                type: types.SETDRIVERINFO,
                isLoadingDriver: true,
                payloadDriver: ''
            });

            return instancenew.post(api.SETDRIVERINFO, queryString.stringify({
                 plateNo, type, make, model, location, page, role
            })
            )
                .then((responseJson) => {
                    return dispatch({
                        type: types.SETDRIVERINFO,
                        isLoadingDriver: false,
                        payloadDriver: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};

export const otherinfo = (dispatch, files, page, role, resumes) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.SETOTHERINFO,
                isLoadingOther: true,
                payloadOther: ''
            });

            let fromData = new FormData();

            files.map((item, i) => {

                let filename = item.purpose + '' + i;
                var extension = item.type.substring(item.type.lastIndexOf('/') + 1, item.type.length);
                fromData.append(filename, {
                    uri: item.uri,
                    name: new Date().getTime()+"."+extension,
                    type: item.type
                });
            }
            );
            fromData.append("role", role);
            fromData.append("page", page);
            fromData.append("agree", 1);
            fromData.append("submit", 1);
            //  console.log(fromData);
            var config = {
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    dispatch({
                        type: types.SETOTHERINFO,
                        isLoadingOther: true,
                        payloadOther: '',
                        progress: percentCompleted
                    });
                    console.log("Progress:-" + percentCompleted);
                }
            };

            return instancenew.post(api.SETDRIVERINFO, fromData, config)

                .then((responseJson) => {
                    return dispatch({
                        type: types.SETOTHERINFO,
                        isLoadingOther: false,
                        payloadOther: responseJson,
                        progress: 100

                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};


export const getDriver = (dispatch) => {
    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.DRIVERINFO,
                isLoadingdriverdata: true,
                payloadAll: ''
            });

            return instancenew.post(api.DRIVERINFO)

                .then((responseJson) => {
                    return dispatch({
                        type: types.DRIVERINFO,
                        isLoadingdriverdata: false,
                        payloadAll: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });
}

export const updateDefaultLocation=(dispatch,lat,lng,location,radius)=>{

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });
            // dispatch({
            //     type: types.IMAGEDEL,
            //     imageDelLoading: true,
            //     payloadImageDel: ''
            // });

            return instancenew.post(api.UPDATELOCATION,queryString.stringify({
                lat,lng,location,radius
                }))
                .then((responseJson) => {
                    return dispatch({
                        type: types.UPDATELOCATION,
                        payloadUpdateLocation: responseJson,
                        isLoading:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });


}

export const getDriverCache = (dispatch) => {
    dispatch({
        type: types.DRIVERINFO
    });
}


export const delImage=(dispatch,position,type,id)=>{
    //alert(position+" "+type+" "+id);

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            // dispatch({
            //     type: types.IMAGEDEL,
            //     imageDelLoading: true,
            //     payloadImageDel: ''
            // });

            return instancenew.post(api.IMAGEDEL,queryString.stringify({
                position,type,id
           }))

                .then((responseJson) => {

                    return dispatch({
                        type: types.IMAGEDEL,
                        payloadImageDel: responseJson,
                        deltype:type,
                        delposition:position
                    });

                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

}

export const notifications=(dispatch)=>{
    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;
            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });

            return instancenew.get(api.NOTIFICATIONLIST)

                .then((responseJson) => {
                    return dispatch({
                        type: types.NOTICATION_LIST_DRIVER,
                        payloadnoti: responseJson,
                        isLoadingnoti:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });
    
}





