import api from '../constants/api';
import instance from './config.js';
const queryString = require('qs');
import * as types from '../actions/actionTypes';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

export const login = (dispatch, email, password) => {

    dispatch({
        type: types.LOGIN,
        isLoading: true,
        payload: ''
    });

    return instance.post(api.LOGIN, queryString.stringify({
        email,
        password
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.LOGIN,
                isLoading: false,
                payload: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
};

export const updateDeviceToken = (dispatch, token) => {
    // alert(token);
    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });


            return instancenew.post(api.UPDATEDEVICETOKEN, queryString.stringify({
                token
            })
            )
                .then((responseJson) => {
                    //alert(JSON.stringify(responseJson));
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

}

export const logout = (dispatch, response) => {
    dispatch({
        type: types.LOGOUT,
        isLoading: false,
        logoutresponse: response
    });



    // return  instance.get(api.LOGOUT)
    //         .then((responseJson) => {
    //         return dispatch({
    //         type: types.LOGOUT,
    //         isLoading:false,
    //         logoutresponse :responseJson
    //     });
    // })
    // .catch((error) => {
    //   console.error(error);
    // });
};

export const forgotPassword = (dispatch, email) => {

    dispatch({
        type: types.FORGOT,
        isLoadingforgot: true,
        payloadforgot: ''
    });

    // alert(email);
    return instance.post(api.FORGOT, queryString.stringify({
        email
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.FORGOT,
                isLoadingforgot: false,
                payloadforgot: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
};


export const resetPassword = (dispatch, password, rpassword, email, otp) => {

    // alert(password+''+rpassword+' '+email+' '+otp);
    dispatch({
        type: types.RESET,
        isLoadingreset: true,
        payloadreset: ''
    });

    // alert(email);
    return instance.post(api.RESET, queryString.stringify({
        password, rpassword, email, otp
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.RESET,
                isLoadingreset: false,
                payloadreset: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
};

export const compareOTP = (dispatch, otp, email) => {

    dispatch({
        type: types.OTP,
        isLoadingotp: true,
        payloadotp: ''
    });
    // alert(email);
    return instance.post(api.OTP, queryString.stringify({
        otp,
        email
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.OTP,
                isLoadingotp: false,
                payloadotp: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });

};

export const emailOtpMatch = (dispatch, id, emailOtp) => {

    dispatch({
        type: types.EMAILOTPMATCH,
        isLoadingmemailotpmatch: true,
        payloademailotpmatch: ''
    });
    // alert(email);
    return instance.post(api.EMAILOTPMATCH, queryString.stringify({
        id,
        emailOtp
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.EMAILOTPMATCH,
                isLoadingmemailotpmatch: false,
                payloademailotpmatch: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });

};


export const phoneOtpMatch = (dispatch, phone, phoneOtp) => {

    dispatch({
        type: types.PHONEOTPMATCH,
        isLoadingphoneotpmatch: true,
        payloadphoneotpmatch: ''
    });
    // alert(email);
    return instance.post(api.PHONEOTPMATCH, queryString.stringify({
        phone,
        phoneOtp
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.PHONEOTPMATCH,
                isLoadingphoneotpmatch: false,
                payloadphoneotpmatch: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });

};

export const resendotp = (dispatch, phone, phoneOtp) => {

    dispatch({
        type: types.RESENDOTP,
        isLoadingresendotp: true,
        payloadresendotp: ''
    });
    // alert(email);
    return instance.post(api.RESENDOTP, queryString.stringify({
        phone,
        phoneOtp
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.RESENDOTP,
                isLoadingresendotp: false,
                payloadresendotp: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });

};

export const resendemail = (dispatch,id,email) => {

    dispatch({
        type: types.RESENDEMAIL,
        isLoadingresendemail: true,
        payloadresendemail: ''
    });
    // alert(email);
    return instance.post(api.RESENDEMAIL, queryString.stringify({
        email
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.RESENDEMAIL,
                isLoadingresendemail: false,
                payloadresendemail: responseJson
            });
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });

};

export const refresh = (dispatch, email) => {

    dispatch({
        type: types.REFRESH,
        payloadrefresh: '',
        isLoadingrefresh: true
    });

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;
            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });

            return instancenew.get(api.REFRESH)
                .then((responseJson) => {
                    return dispatch({
                        type: types.REFRESH,
                        isLoadingrefresh: false,
                        payloadrefresh: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

}

export const signup = (dispatch, phone, email, password, user_type) => {
    dispatch({
        type: types.SIGNUP,
        isLoading: true,
        signupresponse: ''
    });

    return instance.post(api.SIGNUP, queryString.stringify({
        phone,
        email,
        password,
        'rpassword': password,
        'agree': true,
        'is_driver': user_type
    }))

        .then((responseJson) => {
            return dispatch({
                type: types.SIGNUP,
                isLoading: false,
                signupresponse: responseJson
            });
        })
        .catch((error) => {
            console.error(error);
        });
};