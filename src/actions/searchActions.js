import api from '../constants/api';
import instance from './config.js';

import * as types from './actionTypes';

const queryString = require('qs');
export const topSearches = (dispatch) => {
    dispatch({
        type: types.TOPSEARCHES,
        isLoading:true,
        payload :null
    });

    return  instance.get(api.TOPMATCHES)
            .then((responseJson) => {
            return dispatch({
            type: types.TOPSEARCHES,
            isLoading:false,
            payload :responseJson.data.data
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
};


export const favClient = (dispatch,coach_id,disposition) => {
    dispatch({
        type: types.FAVCLIENTS,
    });

    return  instance.post(api.FAVSCLIENT, queryString.stringify({
        coach_id,
        disposition
      }))
   
    .then((responseJson) => {
        return dispatch({
            type: types.FAVCLIENTS
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
};


// export const favClient = (dispatch,coach_id,disposition) => {
//     dispatch({
//         type: types.FAVCLIENTS,
//     });

//     return  instance.post(api.FAVSCLIENT, queryString.stringify({
//         coach_id,
//         disposition
//       }))
   
//     .then((responseJson) => {
//         return dispatch({
//             type: types.FAVCLIENTS
//         });
//     })
//     .catch((error) => {
//       console.error(error);
//     });
// };


export const getFavClient = (dispatch,disposition) => {

   let actionName=types.GETFAVCLIENTS;
   if(disposition==='ignore'){
        actionName=types.GETIGNOREDCLIENTS
   }

   //alert(actionName);

   if(actionName==types.GETFAVCLIENTS){

    //alert("commin here"+actionName);
    dispatch({
        type: actionName,
        isLoading:true,
        payload :null
    });

    return  instance.get(api.FAVSCLIENT+'?disposition='+disposition)

    .then((responseJson) => {
        return dispatch({
            type: actionName,
            isLoading:false,
            payload :responseJson.data.data
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
}

else{
    dispatch({
        type: actionName,
        isLoadingIgnore:true,
        payloadforignore :null
    });
    return  instance.get(api.FAVSCLIENT+'?disposition='+disposition)

    .then((responseJson) => {
        return dispatch({
            type: actionName,
            isLoadingIgnore:false,
            payloadforignore :responseJson.data.data
        });
    })
    .catch((error) => {
      console.log(JSON.stringify(error));
    });
}

};
 
