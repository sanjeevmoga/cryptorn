import api from '../constants/api';
import instance from './config.js';
const queryString = require('qs');
import * as types from '../actions/actionTypes';

import { AsyncStorage } from 'react-native';

import axios from 'axios';


export const personalinfo = (dispatch, name, dob, gender, ssn, profilePic, address, about, page, role, jobexperience,
    experiencetype,firstName,lastName,licenseNo) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.SETPERSONALINFO,
                isLoadingPersonal: true,
                payloadPersonal: ''
            });

            let fromData = new FormData();
            fromData.append("phone", name);
            fromData.append("dob", dob);
            fromData.append("gender", gender);
            fromData.append("ssn", ssn);
            //alert(JSON.stringify(profilePic));
            if(profilePic.uri){
                var extension = profilePic.type.substring(profilePic.type.lastIndexOf('/') + 1, profilePic.type.length);
                fromData.append('profilePic', {
                    uri: profilePic.uri,
                    name: new Date().getTime()+"."+extension,
                    type: profilePic.type
                });
            }
            fromData.append("role", role);
            fromData.append("page", page);
            fromData.append("experienceMon", jobexperience);
            fromData.append("experienceType", experiencetype);
            fromData.append("address", address);
            fromData.append("about", about);
            fromData.append("firstName", firstName);
            fromData.append("lastName", lastName);
            fromData.append("licenseNo", licenseNo);
          
            var config = {
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    console.log("Progress:-" + percentCompleted);
                }
            };

            return instancenew.post(api.SETPERSONALINFO, fromData, config)

                .then((responseJson) => {

                    //alert(JSON.stringify(responseJson));
                    return dispatch({
                        type: types.SETPERSONALINFO,
                        isLoadingPersonal: false,
                        payloadPersonal: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};


export const driverinfo = (dispatch,plateNo, type, make, model, location, page, role) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });
            dispatch({
                type: types.SETDRIVERINFO,
                isLoadingDriver: true,
                payloadDriver: ''
            });

            return instancenew.post(api.SETDRIVERINFO, queryString.stringify({
                 plateNo, type, make, model, location, page, role
            })
            )
                .then((responseJson) => {
                    return dispatch({
                        type: types.SETDRIVERINFO,
                        isLoadingDriver: false,
                        payloadDriver: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};

export const otherinfo = (dispatch, files, page, role, resumes) => {

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.SETOTHERINFO,
                isLoadingOther: true,
                payloadOther: ''
            });

            let fromData = new FormData();

            files.map((item, i) => {

                let filename = item.purpose + '' + i;
                // if(item.purpose==='resume'){

                //     filename=item.purpose;

                // }

                //alert(item.uri);

                //var extension=item.uri.substring(item.uri.lastIndexOf('.')+1, item.uri.length) || item.type.substring(item.type.lastIndexOf('/')+1, item.type.length);
                var extension = item.type.substring(item.type.lastIndexOf('/') + 1, item.type.length);

                fromData.append(filename, {
                    uri: item.uri,
                    name: new Date().getTime()+"."+extension,
                    type: item.type
                });
            }
            );
            fromData.append("role", role);
            fromData.append("page", page);
            fromData.append("agree", 1);
            fromData.append("submit", 1);
            //  console.log(fromData);
            var config = {
                onUploadProgress: function (progressEvent) {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);

                    dispatch({
                        type: types.SETOTHERINFO,
                        isLoadingOther: true,
                        payloadOther: '',
                        progress: percentCompleted
                    });
                    console.log("Progress:-" + percentCompleted);
                }
            };

            return instancenew.post(api.SETDRIVERINFO, fromData, config)

                .then((responseJson) => {
                    return dispatch({
                        type: types.SETOTHERINFO,
                        isLoadingOther: false,
                        payloadOther: responseJson,
                        progress: 100

                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });

};


export const getDriver = (dispatch) => {
    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,

                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            dispatch({
                type: types.DRIVERINFO,
                isLoadingdriverdata: true,
                payloadAll: ''
            });

            return instancenew.post(api.DRIVERINFO)

                .then((responseJson) => {
                    return dispatch({
                        type: types.DRIVERINFO,
                        isLoadingdriverdata: false,
                        payloadAll: responseJson
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });
}

export const submitSchedule=(dispatch,id,lat,lng,location,radius,sDate,eDate,defaultForAll,setAll,dayOff)=>{

    dispatch({
        type: types.SDSUBMIT,
        sdSubmitResponse: null,
        isLoading:false
    })
    var JWT_TOKEN = ''
    let APINAME='';
    if(id){
        APINAME=api.SDUPDATE;
    }else{
        APINAME=api.SDSUBMIT;
    }
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;
            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }
            });
            return instancenew.post(APINAME,queryString.stringify({
                id,lat,lng,location,radius,sDate,eDate,defaultForAll,setAll,dayOff
                }))
                .then((responseJson) => {
                    return dispatch({
                        type: types.SDSUBMIT,
                        sdSubmitResponse: responseJson,
                        isLoading:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });


}

export const listSchedule=(dispatch,date)=>{
   // alert(date);
    dispatch({
        type: types.SDLIST,
        sdListResponse: null,
        isLoading:false
    })
    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;
           // JWT_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiICIsImlkIjoiNWE1OTI2ZjFiMWRiNGUzYjg0MDUyZDMxIiwicm9sZSI6IkRSSVZFUiIsImlhdCI6MTUxNTgwMjkwOX0.kwzdO9Vp7TPE9MrVwAauyJwiQXqEBLCe9T98dXQ24fw';
            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            return instancenew.post(api.SDLIST,queryString.stringify({
                date
                }))
                .then((responseJson) => {
                    return dispatch({
                        type: types.SDLIST,
                        sdListResponse:responseJson,
                        isLoading:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });


}

export const getDriverCache = (dispatch) => {
    dispatch({
        type: types.DRIVERINFO
    });
}


export const deleteSD=(dispatch,id)=>{
    //alert(position+" "+type+" "+id);

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            return instancenew.post(api.SDDEL,queryString.stringify({
                id
           }))

                .then((responseJson) => {
                    return dispatch({
                        type: types.SDDEL,
                        payloaddel: responseJson,
                        isLoading:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });
}

export const setOff=(dispatch,type,date)=>{
    //alert(position+" "+type+" "+id);

    var JWT_TOKEN = ''
    AsyncStorage.getItem("userdata").then((data) => {
        if (data) {
            JWT_TOKEN = JSON.parse(data).token;

            const instancenew = axios.create({
                baseURL: api.BASE_URL,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': JWT_TOKEN
                }

            });

            return instancenew.post(api.SDSETDAYOFF,queryString.stringify({
                type,date
           }))

                .then((responseJson) => {
                   
                    return dispatch({
                        type: types.SDSETDAYOFF,
                        payloadsetdayoff: responseJson,
                        isLoading:false
                    });
                })
                .catch((error) => {
                    console.log(JSON.stringify(error));
                });
        }

    });
}





