import axios from 'axios';
import api from '../constants/api';
const queryString = require('qs');
import { AsyncStorage} from 'react-native';
var JWT_TOKEN=''
 AsyncStorage.getItem("userdata").then((data)=>{
  if(data){
  JWT_TOKEN=JSON.parse(data).token;
  console.log(JWT_TOKEN);
  }
 });

const instance = axios.create({
  baseURL: api.BASE_URL,
  timeout: 60000,
  headers: {
  'Accept': 'application/json', 
  'Content-Type': 'application/x-www-form-urlencoded',
}
});

export default instance;