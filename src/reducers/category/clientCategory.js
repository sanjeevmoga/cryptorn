import * as types from '../../actions/actionTypes';
const initialState = {
    isLoading: true,
    payload:'',
    goalsresponse:'',
    inprogress:false,
    openedSectionsIds:{},
    selectedcategories:[]


  };
const clientCategoryReducer = (state =initialState, action) => {
    switch(action.type) {
        case types.CLIENT_CATEGORY: 
        return Object.assign({}, state, { 
            payload: action.payload,
            isLoading:action.isLoading
        });

        case types.GOALSSETTING: 
        return Object.assign({}, state, { 
            goalsresponse: action.goalsresponse,
            inprogress:action.inprogress
        });

        case types.SETOPENEDSECTIONS:
        return { 
            ...state,
            openedSectionsIds: action.openedSectionsIds
        }

        case types.SETSELECTEDCATEGORIES:

  //  alert(JSON.stringify(state.selectedcategories));

        return { 
            ...state,
            selectedcategories: state.selectedcategories.push(action.cat_id)
        }

    default:
        return state;
    }
  }
  
  export default clientCategoryReducer;