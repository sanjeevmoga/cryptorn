import * as types from '../actions/actionTypes';
const initialState = {
    isLoading: false,
    sdSubmitResponse:null,
    sdListResponse:null,
    sbSetOffResponse:null,
    payloaddel:null,
    payloadsetdayoff:null
  };
const sdReducer = (state =initialState, action) => {
    switch(action.type) {
        case types.SDSUBMIT: 
        return Object.assign({}, state, { 
            sdSubmitResponse: action.sdSubmitResponse,
            isLoading:action.isLoading
        });
        case types.SDLIST: 
        return Object.assign({}, state, { 
            sdListResponse: action.sdListResponse,
            isLoading:action.isLoading
        });

        case types.SDDEL: 
        return Object.assign({}, state,{ 
            payloaddel: action.payloaddel,
            isLoading:action.isLoading
        });

        case types.SDSETDAYOFF: 
        return Object.assign({}, state,{ 
            payloadsetdayoff: action.payloadsetdayoff,
            isLoading:action.isLoading
        });

    default:
        return state;
    }
  
  }

  export default sdReducer;