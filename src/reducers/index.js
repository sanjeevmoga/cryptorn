// reducers/index.js

import { combineReducers } from 'redux';
import routes from './routes';
import authReducer from './auth';
import clientCategory from './category/clientCategory';
import topsearchReducer from './search/topsearches';
import UserReducer from './users';
import driverReducer from './driver';
import sdReducer from './scheduler';
//import { reducer as formReducer } from 'redux-form';
// ... other reducers

export default combineReducers({
  routes,
  'auth':authReducer,
  'clientcategory':clientCategory,
  'topsearchReducer':topsearchReducer,
  UserReducer,
  driverReducer,
  sdReducer
});