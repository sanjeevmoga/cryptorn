import * as types from '../actions/actionTypes';
const initialState = {
    isLoading: false,
    payload:'',
    logoutresponse:'',
    signupresponse:'',
    isLoadingforgot:false,
    payloadforgot :'',
    isLoadingotp:false,
    payloadotp :'',
    isLoadingphoneotpmatch:false,
    payloadphoneotpmatch :'',
    payloadphoneotpmatch: '',
    isLoadingphoneotpmatch:false,
    isLoadingresendotp:false,
    payloadresendotp :'',
    isLoadingresendemail:false,
    payloadresendemail:'',
    payloadrefresh: '',
    isLoadingrefresh:false
  };
const authReducer = (state =initialState, action) => {
    switch(action.type) {

        case types.SIGNUP: 
        return Object.assign({}, state, { 
            signupresponse: action.signupresponse,
            isLoading:action.isLoading
        });
        case types.LOGIN: 
        return Object.assign({}, state, { 
            payload: action.payload,
            isLoading:action.isLoading
        });

        case types.FORGOT: 
        return Object.assign({}, state, { 
            payloadforgot: action.payloadforgot,
            isLoadingforgot:action.isLoadingforgot
        });
    case types.LOGOUT:
        
        return Object.assign({}, state, { 
            logoutresponse: action.logoutresponse,
            isLoading:action.isLoading
        });
        //return initialState;

        case types.OTP:
        
        return Object.assign({}, state, { 
            payloadotp: action.payloadotp,
            isLoadingotp:action.isLoadingotp
        });

        case types.RESET:
        return Object.assign({}, state, { 
            payloadreset: action.payloadreset,
            isLoadingreset:action.isLoadingreset
        });

        case types.EMAILOTPMATCH:
        return Object.assign({}, state,{ 
            payloademailotpmatch: action.payloademailotpmatch,
            isLoadingmemailotpmatch:action.isLoadingmemailotpmatch
        });


        case types.PHONEOTPMATCH:
        return Object.assign({}, state,{ 
            payloadphoneotpmatch: action.payloadphoneotpmatch,
            isLoadingphoneotpmatch:action.isLoadingphoneotpmatch
        });

        case types.RESENDOTP:
        return Object.assign({}, state, { 
            payloadresendotp: action.payloadresendotp,
            isLoadingresendotp:action.isLoadingresendotp
        });

        case types.RESENDEMAIL:
        return Object.assign({}, state, { 
            payloadresendemail: action.payloadresendemail,
            isLoadingresendemail:action.isLoadingresendemail
        });

        case types.REFRESH:
        return Object.assign({}, state, { 
            payloadrefresh: action.payloadrefresh,
            isLoadingrefresh:action.isLoadingrefresh
        });


    default:
        return state;
    }
  
  }

  export default authReducer;