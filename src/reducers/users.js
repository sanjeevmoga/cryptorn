import * as Action from './constant';

const UserReducer = (state = { isLoading: false, error: undefined, data: {} }, action) => {
  switch (action.type) {
    case Action.loginData:
      return Object.assign({},state,{data:action.data});
      case Action.cataClientData:
        return Object.assign({},state,{data:action.data});
        case Action.accountCoach:
        return Object.assign({},state,{data:action.data});

    default:
          return state;
  }
}
export default UserReducer;
