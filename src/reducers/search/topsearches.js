import * as types from '../../actions/actionTypes';
const initialState = {
    isLoading: true,
    payload:null,
    payloadforignore:null,
    isLoadingIgnore:true
  };
const topsearchReducer = (state =initialState, action) => {
    switch(action.type) {
        case types.TOPSEARCHES: 
        return Object.assign({}, state, { 
            payload: action.payload,
            isLoading:action.isLoading
        });

        case types.FAVCLIENTS: 
        return Object.assign({}, state, { 
            
        });

        case types.GETFAVCLIENTS: 
        return Object.assign({}, state, { 
            payload: action.payload,
            isLoading:action.isLoading
        });

        case types.GETIGNOREDCLIENTS: 
        return Object.assign({}, state, { 
            payloadforignore: action.payloadforignore,
            isLoadingIgnore:action.isLoadingIgnore
        });

    default:
        return state;
    }
  }
  export default topsearchReducer;