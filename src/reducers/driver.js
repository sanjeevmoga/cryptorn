import * as types from '../actions/actionTypes';
const initialState = {
    isLoadingPersonal: false,
    payloadPersonal:'',
    isLoadingDriver: false,
    payloadDriver:'',

    isLoadingOther: false,
    payloadOther:'',
    progress:0,
    isLoadingdriverdata: false,
    payloadAll:'',
    payloadImageDel:'',
    deltype:'',
    delposition:-1,
    payloadUpdateLocation:'',
    refresh:false,
    payloadnoti:'',
    isLoadingnoti:false,
    payloadmarkitread:'',
    isLoadingmarkeitread:false

  };
const driverReducer = (state =initialState, action) => {
    switch(action.type) {

        case types.SETPERSONALINFO: 
        return Object.assign({}, state, { 
            payloadPersonal: action.payloadPersonal,
            isLoadingPersonal:action.isLoadingPersonal
        });

        case types.SETDRIVERINFO: 
        return Object.assign({}, state, { 
            payloadDriver: action.payloadDriver,
            isLoadingDriver:action.isLoadingDriver
        });
        
        case types.SETOTHERINFO: 
        return Object.assign({}, state, { 
            payloadOther: action.payloadOther,
            isLoadingOther:action.isLoadingOther,
            progress:action.progress,
        });

        case types.IMAGEDEL: 
        return Object.assign({}, state, { 
            payloadImageDel: action.payloadImageDel,
            deltype:action.deltype,
            delposition:action.delposition
        });
        case types.UPDATELOCATION: 
        return Object.assign({}, state, { 
            payloadUpdateLocation: action.payloadUpdateLocation,
            isLoading:action.isLoading
        });

        case types.REFRESHPROFILE: 
        return Object.assign({}, state,{ 
            refresh: action.refresh
        });

        case types.DRIVERINFO: 
        //alert(action.payloadAll);
        return Object.assign({}, state, { 
            payloadAll: action.payloadAll===undefined?state.payloadAll:action.payloadAll,
            isLoadingdriverdata:action.isLoadingdriverdata,
            refresh:false
        });

        case types.NOTICATION_LIST_DRIVER: 
        return Object.assign({}, state, { 
            payloadnoti: action.payloadnoti,
            isLoadingnoti:action.isLoadingnoti
        });

        case types.NOTICATION_MARK_IT_READ: 
        return Object.assign({}, state, { 
            payloadmarkitread: action.payloadmarkitread,
            isLoadingmarkeitread:action.isLoadingmarkeitread
        });
        
    //     case types.LOGIN: 
    //     return Object.assign({}, state, { 
    //         payload: action.payload,
    //         isLoading:action.isLoading
    //     });
    // case types.LOGOUT:
    //      state = undefined;
    //     return Object.assign({}, state, { 
    //         logoutresponse: action.logoutresponse,
    //         isLoading:action.isLoading
    //     });
    
    default:
        return state;
    }
  
  }

  export default driverReducer;