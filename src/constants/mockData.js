/**
 * Created by deepak on 06/11/2017.
 */

 const chooseCategoryClient = [
      {
        header: {
          title: 'Life Coaching',
          image: require('../res/images/heart.png'),
          expanded: true,
        },
        data: [{
          title: 'Finding what i want to do in life',
          checked: false
        },
        {
          title: 'Getting organise and feeling in control',
          checked: false
        },
        {
          title: 'Improving health & work-life balance',
          checked: false
        },
        {
          title: 'Mastering personal spending',
          checked: false
        },
        {
          title: 'Increase confidence & self esteem',
          checked: false
        },
        {
          title: 'Build inner strength',
          checked: false
        },
        {
          title: 'Increase motivation and fulfillment',
          checked: false
        },
        {
          title: 'Increase happiness',
          checked: false
        }]
      },
      {
        header: {
          title: "Weight Loss Coaching",
          image: require('../res/images/weight_loss.png'),
          expanded: false
        },
        data: [{
          title: 'Finding what i want to do in life',
          checked: false
        },
        {
          title: 'Getting organise and feeling in control',
          checked: false
        },
        {
          title: 'Improving health & work-life balance',
          checked: false
        },
        {
          title: 'Mastering personal spending',
          checked: false
        },
        {
          title: 'Increase confidence & self esteem',
          checked: false
        },
        {
          title: 'Build inner strength',
          checked: false
        },
        {
          title: 'Increase motivation and fulfillment',
          checked: false
        },
        {
          title: 'Increase happiness',
          checked: false
        }]
      },
      {
        header: {
          title: "Spiritual Coaching",
          image: require('../res/images/Spiritual.png'),
          expanded: false
        },
        data: [{
          title: 'Finding what i want to do in life',
          checked: false
        },
        {
          title: 'Getting organise and feeling in control',
          checked: false
        },
        {
          title: 'Improving health & work-life balance',
          checked: false
        },
        {
          title: 'Mastering personal spending',
          checked: false
        },
        {
          title: 'Increase confidence & self esteem',
          checked: false
        },
        {
          title: 'Build inner strength',
          checked: false
        },
        {
          title: 'Increase motivation and fulfillment',
          checked: false
        },
        {
          title: 'Increase happiness',
          checked: false
        }]
      },
      {
        header: {
          title: "Retirement Coaching",
          image: require('../res/images/Retirement.png'),
          expanded: false
        },
        data: [{
          title: 'Finding what i want to do in life',
          checked: false
        },
        {
          title: 'Getting organise and feeling in control',
          checked: false
        },
        {
          title: 'Improving health & work-life balance',
          checked: false
        },
        {
          title: 'Mastering personal spending',
          checked: false
        },
        {
          title: 'Increase confidence & self esteem',
          checked: false
        },
        {
          title: 'Build inner strength',
          checked: false
        },
        {
          title: 'Increase motivation and fulfillment',
          checked: false
        },
        {
          title: 'Increase happiness',
          checked: false
        }]
      }
    ];

const data = [
         {
            id: 0,
            name: 'Ben',
            uri: 'https://livecoach-avatar.s3.us-east-2.amazonaws.com/hillaryclinton.jpg',
         },
         {
            id: 1,
            name: 'Susan',
            uri: 'https://livecoach-avatar.s3.us-east-2.amazonaws.com/hillaryclinton.jpg',
         },
         {
            id: 2,
            name: 'Robert',
             uri: 'https://livecoach-avatar.s3.us-east-2.amazonaws.com/hillaryclinton.jpg',
         },
         {
            id: 3,
            name: 'Mary',
             uri: 'https://livecoach-avatar.s3.us-east-2.amazonaws.com/hillaryclinton.jpg',
         },
         {
            id: 4,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 5,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 6,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 7,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 8,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 9,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 10,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 11,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 12,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 13,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 14,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 15,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
         {
            id: 16,
            name: 'Mary',
             uri: 'https://pbs.twimg.com/profile_images/898363296373325824/C1skdBx3_400x400.jpg',
         },
      ];

export default {
  chooseCategoryClient,
  data,
};
