let BASE_URL='http://yourserver.com:8084/api/';
//let BASE_URL='http://10.20.3.160:8055/api/';

const api = {
    BASE_URL:`${BASE_URL}`,
    SIGNUP : `${BASE_URL}users/`,
    LOGIN  : `${BASE_URL}users/login`,
    LOGOUT  : `${BASE_URL}users/login/logout`,
    CATEGORIES  : `${BASE_URL}users/categories`,
    ACCOUNT  : `${BASE_URL}users/account/user`,
    TOPMATCHES :`${BASE_URL}users/search/top_matches`,
    FAVSCLIENT :`${BASE_URL}users/favs/client`,
    SETPERSONALINFO :`${BASE_URL}users/profile`,
    IMAGEDEL :`${BASE_URL}users/delImage`,
    SETDRIVERINFO :`${BASE_URL}users/profile`,
    FORGOT:`${BASE_URL}users/forgotPassword`,
    DRIVERINFO:`${BASE_URL}users/getDriver`,
    OTP:`${BASE_URL}users/otpMatch`,
    RESENDOTP:`${BASE_URL}users/phoneResendRegistration`,
    RESENDEMAIL:`${BASE_URL}users/emailResendRegistration`,
    EMAILOTPMATCH:`${BASE_URL}users/emailOtpMatch`,
    PHONEOTPMATCH:`${BASE_URL}users/phoneOtpMatch`,
    RESET:`${BASE_URL}users/resetPassword`,
    REFRESH:`${BASE_URL}users/getVerifiedDetails`,
    UPDATELOCATION:`${BASE_URL}drivers/updateLocation`,
    UPDATEDEVICETOKEN:`${BASE_URL}users/updateToken`,
    SDSUBMIT:`${BASE_URL}schedular/add`,
    SDUPDATE:`${BASE_URL}schedular/update`,
    SDLIST:`${BASE_URL}schedular/list`,
    SDDEL:`${BASE_URL}schedular/delete`,
    SDSETDAYOFF:`${BASE_URL}schedular/setDay`,
    NOTIFICATIONLIST:`${BASE_URL}notifications/`,
    
};
export default api;
