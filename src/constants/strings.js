/**
 * Created by Deepak on 06/11/2017.
 */

const strings = {
	done: 'DONE',
	seeCoaches: 'See Coaches',
	tellUs: 'Tell Us ',
	whatYouWantToAchieve: 'What You Want To Achieve',
	bestCoachText: 'We\'ll Find You The Perfect Coach',
	personalCoaching: 'Personal Coaching',
	profesionalCoaching: 'Professional Coaching',
	tellUsMore: 'Tell Us More About What You Want To Achieve',
};

export default strings;
