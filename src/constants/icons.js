/**
 * Created by Deepak on 06/11/2017.
 */

const icons = {
	up: require('../res/images/up.png'),
	down: require('../res/images/down.png'),
	check: require('../res/images/check.png'),
	uncheck: require('../res/images/uncheck.png'),
	eye: require('../res/images/eye.png'),
	badge: require('../res/images/badge.png'),
	mail: require('../res/images/mail.png'),
	star: require('../res/images/star.png'),
	star_gray: require('../res/images/star_gray.png'),
	star_half: require('../res/images/star_half.png'),
	eye_close: require('../res/images/eye_close.png'),
	swipe_icon: require('../res/images/swipe_icon.png'),
	heart_circle: require('../res/images/heart_circle.png'),
	leftArrow: require('../res/images/leftarrow.png'),
	rightArrow: require('../res/images/rightarrow.png'),
	appLogo: require('../res/images/NavLogo.png'),
	bluelogo: require('../res/images/bluelogo.png'),
	onboarding:require('../res/images/onboarding.png'),
	backgroundone: require('../res/images/backgroundone.png'),
	backgroundtwo:require('../res/images/backgroundtwo.png'),
	loginbackground:require('../res/images/abstract_img.jpg'),
	splash:require('../res/images/splash.png'),
	pattern:require('../res/images/pattern.png'),
	hide:require('../res/images/hide.png'),
	show:require('../res/images/view.png'),
	hamburger:require('../res/images/ic_menu.png'),
	// tab icons 
	ic_home: require('../res/images/ic_home.png'),
	ic_home_select: require('../res/images/ic_home_select.png'),

	ic_market: require('../res/images/ic_account.png'),
	ic_market_select: require('../res/images/ic_account_select.png'),

	ic_exchange: require('../res/images/ic_exchange.png'),
	ic_exchange_select: require('../res/images/ic_exchange_select.png'),

	ic_order: require('../res/images/ic_order.png'),
	ic_order_select: require('../res/images/ic_order_select.png'),
	icon01: require('../res/images/icon01.png'),
	icon02: require('../res/images/icon02.png'),
	icon03: require('../res/images/icon03.png'),
	icon04: require('../res/images/icon04.png'),
	icon05: require('../res/images/icon05.png'),
	ic_notify:require('../res/images/ic_notify.png'),
	ic_back_arrow:require('../res/images/ic_back_arrow.png'),
	ic_logo_with_text:require('../res/images/logo_with_text.png'),
	ic_close_round:require('../res/images/ic_close_round.png'),
	
};

export default icons;
