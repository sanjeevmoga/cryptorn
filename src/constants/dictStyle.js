/**
 * Created by Deepak on 06/11/2017.
 */

import { 
  Dimensions
 } from 'react-native';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const colorSetData = {
  pageBackground: '#ffffff',
  mainText: '#2a2f43',
  secondaryTextColor: '#db4f8a',
  buttonBackground: '#2a2f43',
  white: '#ffffff',
  headerBorder: '#E0E0E0',
  itemBackground: '#FAFAFA',
  lightText: '#B4B6C3',
  blueColor:'#6a47d5',
  greyColor:'grey',
  accentColor:'#FF4081'
};

const fontSetData = {
  bSize: 16,
  fsize: 15,
  mSize: 13,
  xSize: 11
};

const StyleDict = {
  colorSet: colorSetData,
  fontSet: fontSetData,
  windowW: WINDOW_WIDTH,
  windowH: WINDOW_HEIGHT
};

export default StyleDict;
