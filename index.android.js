/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

// import React, { Component } from 'react';
// import {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View
// } from 'react-native';

// import App from './src/Components/App';

// export default class twentyfoursevens extends Component {
//   render() {
//     return (
//       <App/>
//     );
//   }
// }

// AppRegistry.registerComponent('twentyfoursevens', () => twentyfoursevens);

import { AppRegistry } from 'react-native';
import App from './src/Components/App';
AppRegistry.registerComponent('twentyfoursevens', () => App);